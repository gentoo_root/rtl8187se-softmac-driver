#pragma once

#include "global.h"

void rtl_op_tx(struct ieee80211_hw *hw, struct ieee80211_tx_control *control, struct sk_buff *skb);
void rtl_op_flush(struct ieee80211_hw *hw, bool drop);
void rtl_tx_beacon(struct rtl_priv *priv);
void rtl_isr_tx(struct rtl_priv *priv, int priority);
