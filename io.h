#pragma once

#include "global.h"
#include "bitops.h"

/*
 * NIC registers
 */

static inline u8 rtl_ioread8(struct rtl_priv *priv, uintptr_t offset)
{
	return ioread8((u8 __iomem *)priv->iomem + offset);
}

static inline u16 rtl_ioread16(struct rtl_priv *priv, uintptr_t offset)
{
	return ioread16((u8 __iomem *)priv->iomem + offset);
}

static inline u32 rtl_ioread32(struct rtl_priv *priv, uintptr_t offset)
{
	return ioread32((u8 __iomem *)priv->iomem + offset);
}

static inline void rtl_iowrite8(struct rtl_priv *priv, uintptr_t offset, u8 data)
{
	iowrite8(data, (u8 __iomem *)priv->iomem + offset);
	mmiowb();
}

static inline void rtl_iowrite16(struct rtl_priv *priv, uintptr_t offset, u16 data)
{
	iowrite16(data, (u8 __iomem *)priv->iomem + offset);
	mmiowb();
}

static inline void rtl_iowrite32(struct rtl_priv *priv, uintptr_t offset, u32 data)
{
	iowrite32(data, (u8 __iomem *)priv->iomem + offset);
	mmiowb();
}

static inline void rtl_iosetbit8(struct rtl_priv *priv, uintptr_t offset, u8 bit, bool state)
{
	u8 reg = rtl_ioread8(priv, offset);
	if (state)
		reg |= bit;
	else
		reg &= ~bit;
	rtl_iowrite8(priv, offset, reg);
}

static inline void rtl_iosetbit16(struct rtl_priv *priv, uintptr_t offset, u16 bit, bool state)
{
	u16 reg = rtl_ioread16(priv, offset);
	if (state)
		reg |= bit;
	else
		reg &= ~bit;
	rtl_iowrite16(priv, offset, reg);
}

static inline void rtl_iosetbit32(struct rtl_priv *priv, uintptr_t offset, u32 bit, bool state)
{
	u32 reg = rtl_ioread32(priv, offset);
	if (state)
		reg |= bit;
	else
		reg &= ~bit;
	rtl_iowrite32(priv, offset, reg);
}
