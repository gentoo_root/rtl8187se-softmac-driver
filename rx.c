#include "rx.h"
#include "desc.h"
#include <linux/etherdevice.h>

static s8 rtl_calc_signal(struct rtl_rx_desc *desc)
{
	const u8 lna_gain[] = { 2, 17, 29, 39 };
	u8 lna = SHIFTOUT(desc->agc, 0xc0);
	u8 bb = SHIFTOUT(desc->agc, 0x3e);
	int value = -(lna_gain[lna] + bb*2);
	return value+4;
}

static void rtl_rx_collect_stats(struct rtl_priv *priv, struct rtl_rx_desc *desc, struct sk_buff *skb, int rate_idx, int rxagc)
{
	struct ieee80211_hdr *hdr = (struct ieee80211_hdr *)skb->data;
	__le16 fc = hdr->frame_control;
	u32 flags = le32_to_cpu(desc->flags);

	if (unlikely(flags & (BIT_RX_DESC_FLAG_CRC32_ERR | BIT_RX_DESC_FLAG_ICV_ERR)))
		return;

	if (!priv->vif)
		return;

	if (ieee80211_is_ctl(fc))
		return;

	if (ether_addr_equal(priv->vif->bss_conf.bssid,
		ieee80211_has_tods(fc) ? hdr->addr1 :
		ieee80211_has_fromds(fc) ? hdr->addr2 :
		hdr->addr3)) {
		enum rtl_antenna antenna;

		if (likely(priv->dm.smoothed_signal_strength >= 0))
			priv->dm.smoothed_signal_strength = (priv->dm.smoothed_signal_strength*5 + rxagc*10) / 6;
		else
			priv->dm.smoothed_signal_strength = rxagc*10;
		msg_dbg(priv, "Signal strength %d\n", rxagc);
		msg_dbg(priv, "Smoothed signal strength %d\n", priv->dm.smoothed_signal_strength);

		/* High power mechanism */
		priv->dm.last_cck = rate_idx <= 3;
		priv->dm.last_cck_rssi = priv->dm.last_cck ? desc->rssi & 0x7f : 0;

		/* Antenna diversity */
		antenna = SHIFTOUT(desc->rssi, BIT(7)) ? RTL_ANTENNA_MAIN : RTL_ANTENNA_AUX;
		if (likely(priv->dm.ad_rx_ss >= 0))
			priv->dm.ad_rx_ss = (priv->dm.ad_rx_ss*7 + rxagc*3) / 10;
		else
			priv->dm.ad_rx_ss = rxagc;
		priv->dm.rx_ok_cnt++;
		priv->dm.rx_ok_cnt_antenna[antenna]++;
	}
}

void rtl_isr_rx(struct rtl_priv *priv)
{
	struct rtl_rx_ring *ring = &priv->rx[0];
	size_t count = RTL_RX_BUF_COUNT;
	size_t index = ring->idx;
	size_t cnt = 0;

	while (count--) {
		struct rtl_rx_desc *desc = &ring->desc[index];
		struct sk_buff *skb = ring->buf[index];
		struct sk_buff *new_skb;
		u32 flags = le32_to_cpu(desc->flags);
		struct ieee80211_rx_status rx_status = {0};
		int rxagc;

		if (flags & BIT_RX_DESC_FLAG_OWN)
			/* wait data to be filled by hw */
			break;

		if (unlikely(flags & (BIT_RX_DESC_FLAG_DMA_FAIL | BIT_RX_DESC_FLAG_FOF | BIT_RX_DESC_FLAG_RX_ERR)))
			goto rtl_isr_rx_done;

		/* RX status */

		rx_status.antenna = SHIFTOUT(desc->rssi, BIT(7));
		rx_status.signal = rtl_calc_signal(desc);
		rx_status.rate_idx = SHIFTOUT(flags, MASK_RX_DESC_RATE_IDX);
		rx_status.freq = priv->hw->conf.channel->center_freq;
		rx_status.band = priv->hw->conf.channel->band;
		rx_status.mactime = le64_to_cpu(desc->mac_time);
		rx_status.flag |= RX_FLAG_MACTIME_START;
		if (unlikely(flags & BIT_RX_DESC_FLAG_CRC32_ERR))
			rx_status.flag |= RX_FLAG_FAILED_FCS_CRC;

		rxagc = rx_status.rate_idx <= 3 ?	/* CCK rate */
			(95-clamp(-rx_status.signal, 30, 95))*100/65 :
			(90-clamp(-rx_status.signal, 25, 90))*100/65;
		rtl_rx_collect_stats(priv, desc, skb, rx_status.rate_idx, rxagc);
		msg_dbg(priv, "Rate index %d\n", rx_status.rate_idx);

		/* Get frame to mac80211 */

		new_skb = dev_alloc_skb(RTL_RX_BUF_SIZE);
		if (unlikely(!new_skb)) {
			msg_err(priv, "unable to alloc RX skb\n");
			goto rtl_isr_rx_done;
		}

		// TODO: maybe somehow avoid remapping?
		// using pci_dma_sync_single_for_{cpu,device}
		pci_unmap_single(priv->pdev, *((dma_addr_t *)skb->cb), RTL_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);

		skb_put(skb, flags & 0xfff);

		memcpy(IEEE80211_SKB_RXCB(skb), &rx_status, sizeof(rx_status));
		ieee80211_rx_irqsafe(priv->hw, skb);

		skb = new_skb;
		ring->buf[index] = skb;
		*((dma_addr_t *)skb->cb) = pci_map_single(priv->pdev, skb_tail_pointer(skb), RTL_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);

		/* Refresh descriptor */

rtl_isr_rx_done:
		desc->rx_buf = cpu_to_le32(*((dma_addr_t *)skb->cb));
		desc->flags = cpu_to_le32(BIT_RX_DESC_FLAG_OWN | SHIFTIN(RTL_RX_BUF_SIZE, MASK_RX_DESC_BUF_SIZE));
		if (index == RTL_RX_BUF_COUNT-1)
			desc->flags |= cpu_to_le32(BIT_RX_DESC_FLAG_EOR);
		index = (index + 1) % RTL_RX_BUF_COUNT;
		cnt++;
	}

	ring->idx = index;

	msg_dbg(priv, "RX ISR: RXed %u packets\n", cnt);
}
