#pragma once

#include "bitops.h"

/*
 * NIC registers
 */

/*
 * Interrupt Status Register
 */
#define REG_ISR				0x3c
/* Bits are the same as in IMR */

/*
 * Transmit Config Register
 */
#define REG_TX_CONF			0x40
/* Data packet retry limit */
#define MASK_TX_CONF_LRL		BITS(0, 7)
/* RTS retry limit */
#define MASK_TX_CONF_SRL		BITS(8, 15)
/* Append CRC32 to the end of packet */
#define BIT_TX_CONF_CRC			BIT(16)
/* Loopback test */
#define MASK_TX_CONF_LOOPBACK		BITS(17, 18)
#define BIT_TX_CONF_LOOPBACK_NONE	SHIFTIN(0, MASK_TX_CONF_LOOPBACK) /* Normal operation */
#define BIT_TX_CONF_LOOPBACK_MAC	SHIFTIN(1, MASK_TX_CONF_LOOPBACK) /* MAC loopback */
#define BIT_TX_CONF_LOOPBACK_BASEBAND	SHIFTIN(2, MASK_TX_CONF_LOOPBACK) /* Baseband loopback */
#define BIT_TX_CONF_LOOPBACK_CONT	SHIFTIN(3, MASK_TX_CONF_LOOPBACK) /* Continue TX */
/* Don't append ICV to the end of encipherment packet */
#define BIT_TX_CONF_NO_ICV		BIT(19)
/* Disable contention window backoff */
#define BIT_TX_CONF_DISCW		BIT(20)
/* Max DMA burst size per TX DMA burst */
#define MASK_TX_CONF_MXDMA		BITS(21, 23)
#define BIT_TX_CONF_MXDMA_16		SHIFTIN(0, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_32		SHIFTIN(1, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_64		SHIFTIN(2, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_128		SHIFTIN(3, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_256		SHIFTIN(4, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_512		SHIFTIN(5, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_1024		SHIFTIN(6, MASK_TX_CONF_MXDMA)
#define BIT_TX_CONF_MXDMA_2048		SHIFTIN(7, MASK_TX_CONF_MXDMA)
/* Set ACK Timeout: The EIFS, ACK and CTS timeouts are derived from the
 * following equation:
 * EIFS = 112/ACKrate + 252
 * 1: ACKrate is dependent on the maximum of MBR (bits 1:0, BRSR) and RX
 * DATA/RTS rate.
 * 0: ACKrate is fixed at 1Mbps. */
#define BIT_TX_CONF_SAT_HWPLCP		BIT(24)
/* Hardware version ID */
#define MASK_TX_CONF_HWVER		BITS(25, 27)
#define BIT_TX_CONF_HWVER_R8180_ABCD	SHIFTIN(2, MASK_TX_CONF_HWVER)
#define BIT_TX_CONF_HWVER_R8180_F	SHIFTIN(3, MASK_TX_CONF_HWVER)
#define BIT_TX_CONF_HWVER_R8185_ABC	SHIFTIN(4, MASK_TX_CONF_HWVER)
#define BIT_TX_CONF_HWVER_R8185_D	SHIFTIN(5, MASK_TX_CONF_HWVER)
#define BIT_TX_CONF_HWVER_R8187vD	SHIFTIN(5, MASK_TX_CONF_HWVER)
#define BIT_TX_CONF_HWVER_R8187vD_B	SHIFTIN(6, MASK_TX_CONF_HWVER)
/* Unknown */
#define BIT_TX_CONF_DISREQQSIZE		BIT(28)
#define BIT_TX_CONF_PROBE_DTS		BIT(29)
/* Software sequence number */
#define BIT_TX_CONF_SW_SEQNUM		BIT(30)
/* Contention window minimum value:
 *   0: Cwmin=32
 *   1: Cwmin=8 */
#define BIT_TX_CONF_CW_MIN		BIT(31)

/*
 * Receive Config Register
 */
#define REG_RX_CONF			0x44
/* Accept all packets with destination address */
#define BIT_RX_CONF_MONITOR		BIT(0)
/* Accept physical match packets */
#define BIT_RX_CONF_NICMAC		BIT(1)
/* Accept multicast packets */
#define BIT_RX_CONF_MULTICAST		BIT(2)
/* Accept broadcast packets */
#define BIT_RX_CONF_BROADCAST		BIT(3)
/* Accept CRC32 error packets */
#define BIT_RX_CONF_FCS			BIT(5)
/* EEPROM type: 1 is 93C56, 0 is 93C46 */
#define BIT_RX_CONF_EEPROM		BIT(6)
/* Max DMA burst size per RX DMA burst */
#define MASK_RX_CONF_MXDMA		BITS(8, 10)
#define BIT_RX_CONF_MXDMA_16		SHIFTIN(0, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_32		SHIFTIN(1, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_64		SHIFTIN(2, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_128		SHIFTIN(3, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_256		SHIFTIN(4, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_512		SHIFTIN(5, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_1024		SHIFTIN(6, MASK_RX_CONF_MXDMA)
#define BIT_RX_CONF_MXDMA_UNLIM		SHIFTIN(7, MASK_RX_CONF_MXDMA)
/* Accept ICV error packets */
#define BIT_RX_CONF_ICV			BIT(12)
/* RX FIFO threshold: begin PCI transfer when number of data bytes in RX FIFO
 * reaches this level */
#define MASK_RX_CONF_RX_FIFO		BITS(13, 15)
#define BIT_RX_CONF_RX_FIFO_64		SHIFTIN(2, MASK_RX_CONF_RX_FIFO)
#define BIT_RX_CONF_RX_FIFO_128		SHIFTIN(3, MASK_RX_CONF_RX_FIFO)
#define BIT_RX_CONF_RX_FIFO_256		SHIFTIN(4, MASK_RX_CONF_RX_FIFO)
#define BIT_RX_CONF_RX_FIFO_512		SHIFTIN(5, MASK_RX_CONF_RX_FIFO)
#define BIT_RX_CONF_RX_FIFO_1024	SHIFTIN(6, MASK_RX_CONF_RX_FIFO)
#define BIT_RX_CONF_RX_FIFO_NONE	SHIFTIN(7, MASK_RX_CONF_RX_FIFO)
/* Accept data frames */
#define BIT_RX_CONF_DATA		BIT(18)
/* Accept control frames */
#define BIT_RX_CONF_CTRL		BIT(19)
/* Accept management frames */
#define BIT_RX_CONF_MGMT		BIT(20)
/* Accept address 3 match packets */
#define BIT_RX_CONF_ADDR3		BIT(21)
/* Accept power management packets */
#define BIT_RX_CONF_PM			BIT(22)
/* Check BSSID, To DS, From DS match packet */
#define BIT_RX_CONF_BSSID		BIT(23)
/* Enable MAC autoreset PHY */
#define BIT_RX_CONF_RX_AUTORESETPHY	BIT(28)
/* Enable Carrier Sense Detection Method 1 */
#define BIT_RX_CONF_CSDM1		BIT(29)
/* Enable Carrier Sense Detection Method 2 */
#define BIT_RX_CONF_CSDM2		BIT(30)
/* Perform early receiving only for packets larger than 1536 bytes */
#define BIT_RX_CONF_ONLYERLPKT		BIT(31)

/*
 * Interrupt Mask Register
 */
#define REG_IMR				0x6c
/* Timeout 3 interrupt */
#define BIT_IMR_TIMEOUT3		BIT(0) 
/* Timeout 2 interrupt */
#define BIT_IMR_TIMEOUT2		BIT(1) 
/* RX QoS OK interrupt */
#define BIT_IMR_RQOS_OK			BIT(2) 
/* TX BK descriptor OK interrupt */
#define BIT_IMR_TBK_OK			BIT(3) 
/* TX BK descriptor error interrupt */
#define BIT_IMR_TBK_ERR			BIT(4) 
/* TX BE descriptor OK interrupt */
#define BIT_IMR_TBE_OK			BIT(5) 
/* TX BE descriptor error interrupt */
#define BIT_IMR_TBE_ERR			BIT(6) 
/* RX OK interrupt */
#define BIT_IMR_RX_OK			BIT(7) 
/* RX error interrupt */
#define BIT_IMR_RX_ERR			BIT(8) 
/* TX VI descriptor OK interrupt */
#define BIT_IMR_TVI_OK			BIT(9) 
/* TX VI descriptor error interrupt */
#define BIT_IMR_TVI_ERR			BIT(10)
/* RX descriptor unavailable interrupt */
#define BIT_IMR_RX_UNAVAIL		BIT(11)
/* RX FIFO overflow interrupt */
#define BIT_IMR_RX_OVERFLOW		BIT(12)
/* TX VO descriptor OK interrupt */
#define BIT_IMR_TVO_OK			BIT(13)
/* TX VO descriptor error interrupt */
#define BIT_IMR_TVO_ERR			BIT(14)
/* TX high priority descriptor OK interrupt */
#define BIT_IMR_THP_OK			BIT(15)
/* TX high priority descriptor error interrupt */
#define BIT_IMR_THP_ERR			BIT(16)
/* TX beacon descriptor OK interrupt */
#define BIT_IMR_TBC_OK			BIT(17)
/* TX beacon descriptor error interrupt */
#define BIT_IMR_TBC_ERR			BIT(18)
/* ATIM timeout interrupt */
#define BIT_IMR_ATIM_TIMEOUT		BIT(19)
/* Beacon timeout interrupt */
#define BIT_IMR_BEACON_TIMEOUT		BIT(20)
/* Timeout 1 interrupt */
#define BIT_IMR_TIMEOUT1		BIT(21)
/* TX FIFO overflow interrupt */
#define BIT_IMR_TX_OVERFLOW		BIT(22)
/* Wake up interrupt */
#define BIT_IMR_WAKE			BIT(23)
/* Beacon DMA interrupt */
#define BIT_IMR_BEACON_DMA		BIT(24)
/* 802.11h measurement interrupt */
#define BIT_IMR_DOT11H			BIT(25)
/* TX Manage OK interrupt */
#define BIT_IMR_TMG_OK			BIT(30)
/* TX Manage error interrupt */
#define BIT_IMR_TMG_ERR			BIT(31)
