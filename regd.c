#include "regd.h"

static bool regd_enabled = true;
module_param(regd_enabled, bool, S_IRUGO);

enum rtl_country_code
{
	COUNTRY_CODE_FCC,
	COUNTRY_CODE_IC,
	COUNTRY_CODE_ETSI,
	COUNTRY_CODE_SPAIN,
	COUNTRY_CODE_FRANCE,
	COUNTRY_CODE_MKK,
	COUNTRY_CODE_MKK1,
	COUNTRY_CODE_ISRAEL,
	COUNTRY_CODE_TELEC,
	COUNTRY_CODE_GLOBAL_DOMAIN,
	COUNTRY_CODE_WORLD_WIDE_13_INDEX,
};

static const char *country_iso_name[] = {
	"US", "US", "EC", "EC", "EC", "JP", "JP", "EC", "JP", "EC", "EC",
};

#define RTL_2GHZ_CH01_11 REG_RULE(2412-10, 2462+10, 40, 0, 20, 0)
#define RTL_2GHZ_CH12_13 REG_RULE(2467-10, 2472+10, 40, 0, 20, NL80211_RRF_PASSIVE_SCAN)
#define RTL_2GHZ_CH14    REG_RULE(2484-10, 2484+10, 40, 0, 20, NL80211_RRF_PASSIVE_SCAN | NL80211_RRF_NO_OFDM)
#define RTL_2GHZ_ALL     REG_RULE(2412-10, 2484+10, 40, 0, 20, 0)

static const struct ieee80211_regdomain rtl_regdom_11 = {
	.n_reg_rules = 1,
	.alpha2 = "99",
	.reg_rules = {
		RTL_2GHZ_CH01_11,
	}
};

static const struct ieee80211_regdomain rtl_regdom_13 = {
	.n_reg_rules = 2,
	.alpha2 = "99",
	.reg_rules = {
		RTL_2GHZ_CH01_11,
		RTL_2GHZ_CH12_13,
	}
};

static const struct ieee80211_regdomain rtl_regdom_14 = {
	.n_reg_rules = 3,
	.alpha2 = "99",
	.reg_rules = {
		RTL_2GHZ_CH01_11,
		RTL_2GHZ_CH12_13,
		RTL_2GHZ_CH14,
	}
};

static const struct ieee80211_regdomain rtl_regdom_all = {
	.n_reg_rules = 1,
	.alpha2 = "99",
	.reg_rules = {
		RTL_2GHZ_ALL,
	}
};

static const struct ieee80211_regdomain *rtl_regdomain_select(u8 country_code)
{
	switch (country_code) {
	case COUNTRY_CODE_FCC:
	case COUNTRY_CODE_IC:
		return &rtl_regdom_11;
	case COUNTRY_CODE_ETSI:
	case COUNTRY_CODE_SPAIN:
	case COUNTRY_CODE_FRANCE:
	case COUNTRY_CODE_ISRAEL:
	case COUNTRY_CODE_TELEC:
	case COUNTRY_CODE_WORLD_WIDE_13_INDEX:
		return &rtl_regdom_13;
	case COUNTRY_CODE_MKK:
	case COUNTRY_CODE_MKK1:
		return &rtl_regdom_14;
	case COUNTRY_CODE_GLOBAL_DOMAIN:
	default:
		return &rtl_regdom_all;
	}
}

int rtl_regd_init(struct rtl_priv *priv)
{
	u8 code = regd_enabled ? priv->eeprom.country_code : COUNTRY_CODE_GLOBAL_DOMAIN;
	struct wiphy *wiphy = priv->hw->wiphy;
	const struct ieee80211_regdomain *regd = rtl_regdomain_select(code);

	wiphy->flags |= WIPHY_FLAG_CUSTOM_REGULATORY;
	wiphy->flags &= ~WIPHY_FLAG_STRICT_REGULATORY;
	wiphy->flags &= ~WIPHY_FLAG_DISABLE_BEACON_HINTS;

	msg_info(priv, "Setting custom regulatory domain, country code %d\n", code);
	wiphy_apply_custom_regulatory(wiphy, regd);

	if (regd_enabled) {
		msg_info(priv, "Regulatory domain country code hint: %s\n", country_iso_name[code]);
		return regulatory_hint(wiphy, country_iso_name[code]);
	}

	return true;
}
