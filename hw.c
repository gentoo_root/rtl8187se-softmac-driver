#include "hw.h"
#include "io.h"
#include "reg.h"
#include "rf.h"
#include <linux/eeprom_93cx6.h>
#include <linux/delay.h>

/* ID register (contains MAC address) */
#define REG_ID				0x00

/* TX rings addresses */
#define REG_TX_RING_ADDR_MG		0x0c	/* Management */
#define REG_TX_RING_ADDR_BK		0x10	/* Background */
#define REG_TX_RING_ADDR_BE		0x14	/* Best effort */
#define REG_TX_RING_ADDR_VI		0x20	/* Video */
#define REG_TX_RING_ADDR_VO		0x24	/* Voice */
#define REG_TX_RING_ADDR_HP		0x28	/* High priority */
#define REG_TX_RING_ADDR_BC		0x4c	/* Beacon */

/* Time Synchronization Function Timer Register */
#define REG_TSFT			0x18

/* BSSID register */
#define REG_BSSID			0x2e

/* Basic Rate Set Register */
#define REG_BRSR			0x34

/* Command register */
#define REG_CMD				0x37
#define BIT_CMD_TX_ENABLE		BIT(2)
#define BIT_CMD_RX_ENABLE		BIT(3)
/* Reset - disable transmitter and receiver, reinit FIFOs */
#define BIT_CMD_RESET			BIT(4)

/* EEPROM Command Register */
#define REG_EEPROM_CMD			0x50
/* EEPROM pins in program and autoload modes */
#define BIT_EEPROM_CMD_READ		BIT(0)
#define BIT_EEPROM_CMD_WRITE		BIT(1)
#define BIT_EEPROM_CMD_CK		BIT(2)
#define BIT_EEPROM_CMD_CS		BIT(3)
/* Operating mode */
#define MASK_EEPROM_CMD_MODE		BITS(6, 7)
/* Normal operation */
#define BIT_EEPROM_CMD_MODE_NORMAL	SHIFTIN(0, MASK_EEPROM_CMD_MODE)
/* Load default register values from EEPROM */
#define BIT_EEPROM_CMD_MODE_LOAD	SHIFTIN(1, MASK_EEPROM_CMD_MODE)
/* Access EEPROM directly */
#define BIT_EEPROM_CMD_MODE_PROGRAM	SHIFTIN(2, MASK_EEPROM_CMD_MODE)
/* Config registers write enable */
#define BIT_EEPROM_CMD_MODE_CONFIG	SHIFTIN(3, MASK_EEPROM_CMD_MODE)

/* Configuration Register 0 */
#define REG_CONFIG0			0x51
/* Geographic Location */
#define MASK_CONFIG0_GL			BITS(0, 1)
#define BIT_CONFIG0_GL_JP		SHIFTIN(1, MASK_CONFIG0_GL)
#define BIT_CONFIG0_GL_EC		SHIFTIN(2, MASK_CONFIG0_GL)
#define BIT_CONFIG0_GL_US		SHIFTIN(3, MASK_CONFIG0_GL)
/* Auxiliary Power Present Status */
#define BIT_CONFIG0_AUX_STATUS		BIT(3)
/* LED used as GPO enable - status of LED0-1 pins depends on the value of
 * BIT_PSR_LEDGPO0-1 (when BIT_PSR_RFKILL is set) */
#define BIT_CONFIG0_LEDGPO_ENABLE	BIT(4)
/* RF block state (when BIT_PSR_RFKILL is clear) */
#define BIT_CONFIG0_RADIO_STATE		BIT(4)
/* 104-bit hardware WEP implemented */
#define BIT_CONFIG0_WEP104		BIT(6)
/* 40-bit hardware WEP implemented */
#define BIT_CONFIG0_WEP40		BIT(7)

/* Media Status Register */
#define REG_MSR				0x58
/* Network type and link status */
#define MASK_MSR_LINK			BITS(2, 3)
#define BIT_MSR_LINK_NONE		SHIFTIN(0, MASK_MSR_LINK)
#define BIT_MSR_LINK_ADHOC		SHIFTIN(1, MASK_MSR_LINK)
#define BIT_MSR_LINK_INFRA		SHIFTIN(2, MASK_MSR_LINK)
#define BIT_MSR_LINK_MASTER		SHIFTIN(3, MASK_MSR_LINK)
/* Enable Enhanced Distributed Channel Access -
 * must always be set, transmit issues? */
#define BIT_MSR_ENEDCA			BIT(4)

/* Configuration register 3 */
#define REG_CONFIG3			0x59
/* Enable writing analog parameters registers */
#define BIT_CONFIG3_ANAPARAM_WRITE	BIT(6)

/* Configuration register 4 */
#define REG_CONFIG4			0x5a
/* TX power management */
#define BIT_CONFIG4_PWRMGT		BIT(5)
/* Power off: turn off external RF power (excluding VCO) and most of the
 * internal power of RTL8187SE */
#define BIT_CONFIG4_PWROFF		BIT(6)
/* VCO Power down: turn off external RF power (including VCO) and most of the
 * internal power of RTL8187SE */
#define BIT_CONFIG4_VCOPDN		BIT(7)

/* Page Select Register */
#define REG_PSR				0x5e
/* 0: registers 0x84-0xd3 are page 0; 1: registers 0x84-0xd3 are page 1 */
#define BIT_PSR_PAGE			BIT(0)
/* Unicast Wakeup Frame */
#define BIT_PSR_UWF			BIT(1)
/* 0: CONFIG0[4] is RADIO_STATE; 1: CONFIG0[4] is LEDGPO_ENABLE */
#define BIT_PSR_RFKILL			BIT(3)
/* LED0 used as GPO (when BIT_CONFIG0_LEDGPO_ENABLE is set) */
#define BIT_PSR_LEDGPO0			BIT(4)
/* LED1 used as GPO (when BIT_CONFIG0_LEDGPO_ENABLE is set) */
#define BIT_PSR_LEDGPO1			BIT(5)
/* General Purpose Input */
#define BIT_PSR_GPI			BIT(6)
/* General Purpose Output */
#define BIT_PSR_GPO			BIT(7)

/* Beacon Interval Register */
#define REG_BCN_INTVL			0x70

/* ATIM Window Register */
#define REG_ATIM_WND			0x72

/* Beacon Interrupt Interval Register */
#define REG_BCN_INT_INTVL		0x74

/* ATIM Interrupt Interval Register */
#define REG_ATIM_INT_INTVL		0x76

/* Transmission Priority Polling Stop Register */
#define REG_TPPOLLSTOP			0x93

/* TX AGC Control Register */
#define REG_TX_AGC_CTL			0x9c
/* Universal AGC control */
#define BIT_TX_AGC_CTL_UNIVERSAL_AGC	BIT(0)
/* TX antenna including feedback control */
#define BIT_TX_AGC_CTL_UNIVERSAL_ANT	BIT(1)

/* WPA config register */
#define REG_WPA_CONF			0xb0

/* Contention Window Configuration Register */
#define REG_CW_CONF			0xbc
#define BTI_CW_CONF_PERPACKET_CW	BIT(0)
#define BIT_CW_CONF_PERPACKET_RETRY	BIT(1)

/* Auto Rate Fallback Control Register */
#define REG_RATE_FALLBACK		0xbe
/* Auto rate fallback per 2^n retry */
#define MASK_RATE_FALLBACK_AUTO_STEP	BITS(0, 1)
#define BIT_RATE_FALLBACK_AUTO_STEP_0	SHIFTIN(0, MASK_RATE_FALLBACK_AUTO_STEP)
#define BIT_RATE_FALLBACK_AUTO_STEP_1	SHIFTIN(1, MASK_RATE_FALLBACK_AUTO_STEP)
#define BIT_RATE_FALLBACK_AUTO_STEP_2	SHIFTIN(2, MASK_RATE_FALLBACK_AUTO_STEP)
#define BIT_RATE_FALLBACK_AUTO_STEP_3	SHIFTIN(3, MASK_RATE_FALLBACK_AUTO_STEP)
/* Unknown */
#define BIT_RATE_FALLBACK_ENABLE_RTSCTS	BIT(6)
/* Enable auto rate fallback */
#define BIT_RATE_FALLBACK_ENABLE	BIT(7)

/* Configuration register 5 */
#define REG_CONFIG5			0xd8

/* Transmission priority DMA polling register */
#define REG_TPPOLL			0xd9

/* PHY parameter register */
#define REG_PHY_PARAM			0xda

/* Interrupt migration */
#define REG_INT_MIG			0xe2

/* RX ring address */
#define REG_RX_RING_ADDR		0xe4

/* TID to AC mapping register */
#define REG_TID_TO_AC_MAP		0xe8

/* Auto Rate Fallback Register */
#define REG_ARFR			0x1e0

/* Function Event Mask Register */
#define REG_FEMR			0x1d4

static void rtl_set_eeprom_mode(struct rtl_priv *priv, enum rtl_eeprom_mode mode)
{
	rtl_iowrite8(priv, REG_EEPROM_CMD, SHIFTIN(mode, MASK_EEPROM_CMD_MODE));
}

static void rtl_eeprom_read(struct eeprom_93cx6 *eeprom)
{
	struct rtl_priv *priv = eeprom->data;
	u8 reg = rtl_ioread8(priv, REG_EEPROM_CMD);

	eeprom->reg_data_in = reg & BIT_EEPROM_CMD_WRITE;
	eeprom->reg_data_out = reg & BIT_EEPROM_CMD_READ;
	eeprom->reg_data_clock = reg & BIT_EEPROM_CMD_CK;
	eeprom->reg_chip_select = reg & BIT_EEPROM_CMD_CS;
}

static void rtl_eeprom_write(struct eeprom_93cx6 *eeprom)
{
	struct rtl_priv *priv = eeprom->data;
	u8 reg = RTL_EEPROM_MODE_PROGRAM << 6;

	if (eeprom->reg_data_in)
		reg |= BIT_EEPROM_CMD_WRITE;
	if (eeprom->reg_data_out)
		reg |= BIT_EEPROM_CMD_READ;
	if (eeprom->reg_data_clock)
		reg |= BIT_EEPROM_CMD_CK;
	if (eeprom->reg_chip_select)
		reg |= BIT_EEPROM_CMD_CS;

	rtl_iowrite8(priv, REG_EEPROM_CMD, reg);
	rtl_ioread8(priv, REG_EEPROM_CMD);
	udelay(10);
}

void rtl_begin_eeprom(struct rtl_priv *priv, struct eeprom_93cx6 *eeprom)
{
	eeprom->data = priv;
	eeprom->register_read = rtl_eeprom_read;
	eeprom->register_write = rtl_eeprom_write;
	eeprom->width = rtl_ioread32(priv, REG_RX_CONF) & BIT_RX_CONF_EEPROM ?
		PCI_EEPROM_WIDTH_93C56 : PCI_EEPROM_WIDTH_93C46;

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_PROGRAM);
	udelay(10);
}

void rtl_end_eeprom(struct rtl_priv *priv)
{
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

void rtl_unlock_anaparam(struct rtl_priv *priv)
{
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iosetbit8(priv, REG_CONFIG3, BIT_CONFIG3_ANAPARAM_WRITE, true);
	/* Leave in config mode to write anaparam */
}

void rtl_lock_anaparam(struct rtl_priv *priv)
{
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iosetbit8(priv, REG_CONFIG3, BIT_CONFIG3_ANAPARAM_WRITE, false);
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

bool rtl_radio_state(struct rtl_priv *priv)
{
	bool state;

	/* Turn off LED before reading CONFIG0[4] */
	rtl_iosetbit8(priv, REG_PSR, BIT_PSR_RFKILL, false);
	udelay(4);

	/* Read rfkill state */
	state = rtl_ioread8(priv, REG_CONFIG0) & BIT_CONFIG0_RADIO_STATE;

	/* Turn on LED */
	rtl_iosetbit8(priv, REG_PSR, BIT_PSR_RFKILL, true);

	return state;
}

void rtl_set_opmode(struct rtl_priv *priv, enum nl80211_iftype iftype)
{
	static const struct {
		u8 msr;
		const char *name;
	} opmodes[NUM_NL80211_IFTYPES] = {
		[0 ... NL80211_IFTYPE_MAX] = { BIT_MSR_LINK_NONE, NULL },
		[NL80211_IFTYPE_UNSPECIFIED] = { BIT_MSR_LINK_NONE, "no link" },
		[NL80211_IFTYPE_ADHOC] = { BIT_MSR_LINK_ADHOC, "ad-hoc" },
		[NL80211_IFTYPE_STATION] = { BIT_MSR_LINK_INFRA, "station" },
		[NL80211_IFTYPE_AP] = { BIT_MSR_LINK_MASTER, "master" },
	};
	u8 reg;

	if (opmodes[iftype].name)
		msg_dbg(priv, "Setting operating mode: %s\n", opmodes[iftype].name);
	else
		msg_warn(priv, "Unsupported operating mode %d\n", iftype);

	reg = rtl_ioread8(priv, REG_MSR) & ~MASK_MSR_LINK;
	rtl_iowrite8(priv, REG_MSR, reg | opmodes[iftype].msr);
}

void rtl_set_tx_state(struct rtl_priv *priv, bool state)
{
	msg_dbg(priv, "Setting TX state: %s\n", state ? "enabled" : "disabled");
	rtl_iosetbit8(priv, REG_CMD, BIT_CMD_TX_ENABLE, state);
}

void rtl_set_rx_state(struct rtl_priv *priv, bool state)
{
	msg_dbg(priv, "Setting RX state: %s\n", state ? "enabled" : "disabled");
	rtl_iosetbit8(priv, REG_CMD, BIT_CMD_RX_ENABLE, state);
}

static void rtl_update_imr(struct rtl_priv *priv)
{
	u32 imr = 0;
	if (priv->nic.irq_enabled) {
		imr =
			BIT_IMR_RX_OK  | BIT_IMR_RX_ERR  | BIT_IMR_RQOS_OK |
			BIT_IMR_TBK_OK | BIT_IMR_TBK_ERR |
			BIT_IMR_TBE_OK | BIT_IMR_TBE_ERR |
			BIT_IMR_TVI_OK | BIT_IMR_TVI_ERR |
			BIT_IMR_TVO_OK | BIT_IMR_TVO_ERR |
			BIT_IMR_THP_OK | BIT_IMR_THP_ERR |
			BIT_IMR_TBC_OK | BIT_IMR_TBC_ERR |
			BIT_IMR_TMG_OK | BIT_IMR_TMG_ERR |
			BIT_IMR_RX_UNAVAIL |
			BIT_IMR_RX_OVERFLOW | BIT_IMR_TX_OVERFLOW;
		if (priv->nic.beacon_enabled)
			imr |= BIT_IMR_BEACON_TIMEOUT | BIT_IMR_BEACON_DMA;
	}
	
	rtl_iowrite32(priv, REG_IMR, imr);
}

void rtl_set_irq_state(struct rtl_priv *priv, bool state)
{
	msg_dbg(priv, "Setting IRQ state: %s\n", state ? "enabled" : "disabled");

	priv->nic.irq_enabled = state;
	rtl_update_imr(priv);
}

void rtl_set_mac_address(struct rtl_priv *priv, const u8 *mac)
{
	msg_dbg(priv, "Setting MAC address: %pM\n", mac);
	
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iowrite32(priv, REG_ID, le32_to_cpu(*(__le32 *)mac));
	rtl_iowrite16(priv, REG_ID+4, le16_to_cpu(*(__le16 *)(mac+4)));
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

void rtl_set_bssid(struct rtl_priv *priv, const u8 *bssid)
{
	msg_dbg(priv, "Setting BSSID: %pM\n", bssid);
	
	rtl_iowrite32(priv, REG_BSSID, le32_to_cpu(*(__le32 *)bssid));
	rtl_iowrite16(priv, REG_BSSID+4, le16_to_cpu(*(__le16 *)(bssid+4)));
}

u64 rtl_get_tsf(struct rtl_priv *priv)
{
	return rtl_ioread32(priv, REG_TSFT) | (u64)(rtl_ioread32(priv, REG_TSFT+4)) << 32;
}

void rtl_set_beacon_enabled(struct rtl_priv *priv, bool state)
{
	msg_dbg(priv, "Setting beacon enabled state: %s\n", state ? "enabled" : "disabled");

	priv->nic.beacon_enabled = state;

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iosetbit8(priv, REG_TPPOLLSTOP, BIT(RTL_TX_QUEUE_BEACON+1), !state);
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);

	/* Set Interrupt Mask Register */
	rtl_update_imr(priv);
}

void rtl_set_beacon_interval(struct rtl_priv *priv, u16 value)
{
	/* Both registers are 10-bit */
	u16 reg = min_t(u16, value, 0x3ff);

	msg_dbg(priv, "Setting beacon interval: %hu\n", value);
	
	/* XXX: units of beacon interval in NIC are 1024 usecs,
	 * but value is in msecs */
	rtl_iowrite16(priv, REG_BCN_INTVL, reg);
	rtl_iowrite16(priv, REG_BCN_INT_INTVL, reg);
}

void rtl_set_atim_window(struct rtl_priv *priv, u16 value)
{
	/* Both registers are 10-bit */
	u16 reg = min_t(u16, value, 0x3ff);

	msg_dbg(priv, "Setting ATIM window: %hu\n", value);

	rtl_iowrite16(priv, REG_ATIM_WND, reg);
	rtl_iowrite16(priv, REG_ATIM_INT_INTVL, reg);
}

void rtl_set_rx_conf(struct rtl_priv *priv, u32 rx_conf)
{
	msg_dbg(priv, "Setting RX config\n");

	rtl_iowrite32(priv, REG_RX_CONF, priv->nic.rx_conf = rx_conf);
}

void rtl_set_tx_conf(struct rtl_priv *priv, u32 tx_conf)
{
	msg_dbg(priv, "Setting TX config\n");

	/* High 16 bits of Transmit Config Register */
	rtl_iowrite16(priv, REG_TX_CONF+2, SHIFTOUT(tx_conf, BITS(16, 31)));
}

void rtl_set_qos(struct rtl_priv *priv, u8 queue)
{
	static const uintptr_t ac_param_reg[IEEE80211_NUM_ACS] = {
		0xf0, 0xf4, 0xf8, 0xfc
	};
	static const char *ac_name[IEEE80211_NUM_ACS] = {
		"VO", "VI", "BE", "BK"
	};
	struct ieee80211_tx_queue_params *params = &priv->qos_params[queue];
	u32 ac_param =
		SHIFTIN(params->aifs, BITS(0, 7)) |
		SHIFTIN(order_base_2(params->cw_min), BITS(8, 11)) |
		SHIFTIN(order_base_2(params->cw_max), BITS(12, 15)) |
		SHIFTIN(params->txop, BITS(16, 31));

	msg_dbg(priv, "Setting QoS parameters for queue %s: aifs %d cw_min %d cw_max %d txop %d value %08x\n", ac_name[queue], params->aifs, params->cw_min, params->cw_max, params->txop, ac_param);

	rtl_iowrite32(priv, ac_param_reg[queue], ac_param);
}

void rtl_set_retry_limits(struct rtl_priv *priv, u8 retry_short, u8 retry_long)
{
	msg_dbg(priv, "Setting retry limits: SRL %u LRL %u\n", retry_short, retry_long);

	/* Low 16 bits of Transmit Config Register */
	rtl_iowrite16(priv, REG_TX_CONF,
		SHIFTIN(retry_short, MASK_TX_CONF_SRL) |
		SHIFTIN(retry_long, MASK_TX_CONF_LRL));
}

void rtl_set_brsr(struct rtl_priv *priv, u16 brsr)
{
	msg_dbg(priv, "Setting BRSR %04x\n", brsr);
	rtl_iowrite16(priv, REG_BRSR, brsr);
}

void rtl_set_rx_ring(struct rtl_priv *priv)
{
	rtl_iowrite32(priv, REG_RX_RING_ADDR, priv->rx[0].dma);
}

/* TX rings addresses */
static const uintptr_t rtl_tx_ring_addr[] = {
	REG_TX_RING_ADDR_MG,
	REG_TX_RING_ADDR_BK,
	REG_TX_RING_ADDR_BE,
	REG_TX_RING_ADDR_VI,
	REG_TX_RING_ADDR_VO,
	REG_TX_RING_ADDR_HP,
	REG_TX_RING_ADDR_BC,
};

void rtl_set_tx_rings(struct rtl_priv *priv)
{
	size_t i;
	for (i = 0; i < RTL_TX_QUEUE_COUNT; i++)
		rtl_iowrite32(priv, rtl_tx_ring_addr[i], priv->tx[i].dma);
}

size_t rtl_get_tx_ring_pointer(struct rtl_priv *priv, u8 queue)
{
	u32 reg = rtl_ioread32(priv, rtl_tx_ring_addr[queue]);
	return (reg - priv->tx[queue].dma) / sizeof(struct rtl_tx_desc);
}

void rtl_tx_dma(struct rtl_priv *priv, u8 queue)
{
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iowrite8(priv, REG_TPPOLL, BIT(queue+1));
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

static bool rtl_flush_check(struct rtl_priv *priv, u8 queue, int timeout)
{
	struct rtl_tx_ring *ring = &priv->tx[queue];

	while (!skb_queue_empty(&ring->queue) && timeout) {
		msleep(10);
		timeout--;
	}

	return timeout;
}

bool rtl_flush(struct rtl_priv *priv, u8 queue, int timeout)
{
	size_t nic_pointer_old = rtl_get_tx_ring_pointer(priv, queue);

	if (!rtl_flush_check(priv, queue, timeout / 2)) {
		size_t nic_pointer_new = rtl_get_tx_ring_pointer(priv, queue);

		if (nic_pointer_old == nic_pointer_new) {
			msg_err(priv, "TX queue %u got stuck\n", queue);

			// TODO: fix the problem with hanging RF
			/* Force restart RF */
			msg_warn(priv, "Restarting RF\n");
			priv->nic.rf_power = false;
			rtl_rf_set_power_state(priv, true);
			rtl_rf_start(priv);
		}

		if (!rtl_flush_check(priv, queue, timeout / 2)) {
			struct rtl_tx_ring *ring = &priv->tx[queue];
			size_t len = skb_queue_len(&ring->queue);

			msg_warn(priv, "Timeout flushing TX queue %u len %u idx %u:%u:%u\n",
				queue, len, ring->idx, rtl_get_tx_ring_pointer(priv, queue),
				(ring->idx + len) % ring->count);

			return false;
		}
	}

	return true;
}

void rtl_set_power(struct rtl_priv *priv, bool state)
{
	msg_dbg(priv, "Setting power: %s\n", state ? "on" : "off");

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iosetbit8(priv, REG_CONFIG4, BIT_CONFIG4_PWROFF | BIT_CONFIG4_VCOPDN, !state);
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

static const u8 rtl_mac_table[][2] = {
	/* Page 0 */
	{0x0a, 0x72}, {0x5b, 0x42},
	{0x88, 0x54}, {0x8b, 0xb8}, {0x8c, 0x03},
	{0x8d, 0x40}, {0x8e, 0x00}, {0x8f, 0x00}, {0x5b, 0x18}, {0x91, 0x03},
	{0x94, 0x0F}, {0x95, 0x32},
	{0x96, 0x00}, {0x97, 0x07}, {0xb4, 0x22}, {0xdb, 0x00},

	/* Page 1 */
	/* For Flextronics system Logo PCIHCT failure:
	 * 0x1C4-0x1CD set no-zero value to avoid PCI configuration space 0x45[7]=1
	 */
	{0x5e, 0x01},
	{0x58, 0x00}, {0x59, 0x00}, {0x5a, 0x04}, {0x5b, 0x00}, {0x60, 0x24},
	{0x61, 0x97}, {0x62, 0xF0}, {0x63, 0x09}, {0x80, 0x0F}, {0x81, 0xFF},
	{0x82, 0xFF}, {0x83, 0x03},
	{0xC4, 0x22}, {0xC5, 0x22}, {0xC6, 0x22}, {0xC7, 0x22}, {0xC8, 0x22},
	{0xC9, 0x22}, {0xCA, 0x22}, {0xCB, 0x22}, {0xCC, 0x22}, {0xCD, 0x22},
	{0xe2, 0x00},

	/* Page 2 */
	{0x5e, 0x02},
	{0x0c, 0x04}, {0x4c, 0x30}, {0x4d, 0x08}, {0x50, 0x05}, {0x51, 0xf5},
	{0x52, 0x04}, {0x53, 0xa0}, {0x54, 0xff}, {0x55, 0xff}, {0x56, 0xff},
	{0x57, 0xff}, {0x58, 0x08}, {0x59, 0x08}, {0x5a, 0x08}, {0x5b, 0x08},
	{0x60, 0x08}, {0x61, 0x08}, {0x62, 0x08}, {0x63, 0x08}, {0x64, 0x2f},
	{0x8c, 0x3f}, {0x8d, 0x3f}, {0x8e, 0x3f},
	{0x8f, 0x3f}, {0xc4, 0xff}, {0xc5, 0xff}, {0xc6, 0xff}, {0xc7, 0xff},
	{0xc8, 0x00}, {0xc9, 0x00}, {0xca, 0x80}, {0xcb, 0x00},
};

static void rtl_init_mac(struct rtl_priv *priv)
{
	size_t i;
	uintptr_t page = 0;

	/* Basic Rate Set Register */
	rtl_set_brsr(priv, 0x0fff);

	/* Retry limit */
	rtl_iosetbit8(priv, REG_CW_CONF, BIT_CW_CONF_PERPACKET_RETRY, true);

	/* TX Automatic Gain Control */
	rtl_iosetbit8(priv, REG_TX_AGC_CTL, BIT_TX_AGC_CTL_UNIVERSAL_AGC | BIT_TX_AGC_CTL_UNIVERSAL_ANT, false);

	/* Auto Rate Fallback Control */
	rtl_iowrite8(priv, REG_RATE_FALLBACK, BIT_RATE_FALLBACK_ENABLE | BIT_RATE_FALLBACK_AUTO_STEP_1);
	/* Enable 1Mbps-54Mbps rates */
	rtl_iowrite16(priv, REG_ARFR, 0x0fff);

	rtl_iowrite16(priv, REG_FEMR, 0xffff);
	rtl_iowrite8(priv, REG_WPA_CONF, 0);

	/* Configure MAC */
	for (i = 0; i < ARRAY_SIZE(rtl_mac_table); i++) {
		uintptr_t offset = rtl_mac_table[i][0];
		u8 value = rtl_mac_table[i][1];
		if (offset == 0x5e)
			page = value;
		else
			rtl_iowrite8(priv, offset | (page << 8), value);
	}

	rtl_iowrite16(priv, REG_TID_TO_AC_MAP, 0xfa50);
	rtl_iowrite16(priv, REG_INT_MIG, 0x0000);

	/* Prevent TPC to cause CRC error */
	rtl_iowrite32(priv, 0x1f0, 0);
	rtl_iowrite32(priv, 0x1f4, 0);
	rtl_iowrite8(priv, 0x1f8, 0);

	/* Enable DA10 TX power saving */
	rtl_iosetbit8(priv, REG_PHY_PARAM, BIT(2), true);
}

static enum rtl_eeprom_mode rtl_get_eeprom_mode(struct rtl_priv *priv)
{
	return SHIFTOUT(rtl_ioread8(priv, REG_EEPROM_CMD), MASK_EEPROM_CMD_MODE);
}

static bool rtl_reset(struct rtl_priv *priv)
{
	int timeout;

	msg_dbg(priv, "Resetting device\n");

	rtl_iowrite8(priv, REG_CMD, BIT_CMD_RESET);
	for (timeout = 200; timeout > 0; timeout--) {
		if (!(rtl_ioread8(priv, REG_CMD) & BIT_CMD_RESET))
			break;
		msleep(1);
	}
	if (!timeout) {
		msg_err(priv, "Reset timeout\n");
		return false;
	}

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_LOAD);
	for (timeout = 100; timeout > 0; timeout--) {
		if (rtl_get_eeprom_mode(priv) == RTL_EEPROM_MODE_NORMAL)
			break;
		msleep(1);
	}
	if (!timeout) {
		msg_err(priv, "EEPROM autoload timeout\n");
		return false;
	}

	msg_dbg(priv, "Reset success\n");

	return true;
}

int rtl_hw_start(struct rtl_priv *priv)
{
	rtl_set_power(priv, true);
	rtl_reset(priv);
	rtl_init_mac(priv);
	rtl_set_opmode(priv, NL80211_IFTYPE_UNSPECIFIED);

	/* Set ENEDCA bit in Media Status Register - avoid TX problems */
	rtl_iosetbit8(priv, REG_MSR, BIT_MSR_ENEDCA, true);

	/* Clear polling stop register */
	rtl_iowrite8(priv, REG_TPPOLLSTOP, 0);

	/* B-cut use LED1 to control HW RF on/off */
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iosetbit8(priv, REG_CONFIG5, BIT(3), false);
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);

	/* Turn on LED */
	rtl_iosetbit8(priv, REG_PSR, BIT_PSR_RFKILL, true);

	/* Configure RX and TX filters */
	rtl_set_rx_conf(priv,
		BIT_RX_CONF_ONLYERLPKT |
		BIT_RX_CONF_RX_AUTORESETPHY |
		BIT_RX_CONF_MGMT |
		BIT_RX_CONF_DATA |
//		BIT_RX_CONF_CTRL |
		BIT_RX_CONF_BROADCAST |
//		BIT_RX_CONF_MULTICAST |
		BIT_RX_CONF_NICMAC |
//		BIT_RX_CONF_BSSID |
		(MASK_RX_CONF_MXDMA & BIT_RX_CONF_MXDMA_UNLIM) |
		(MASK_RX_CONF_RX_FIFO & BIT_RX_CONF_RX_FIFO_NONE));
	rtl_set_tx_conf(priv,
		BIT_TX_CONF_SW_SEQNUM |
		(MASK_TX_CONF_MXDMA & BIT_TX_CONF_MXDMA_2048));

	/* Initialize descriptor rings */
	rtl_set_rx_ring(priv);
	rtl_set_tx_rings(priv);

	return 0;
}
