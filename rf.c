#include "rf.h"
#include "io.h"
#include "hw.h"
#include "bitops.h"

/*
 * PHY registers
 */

/* PHY address register */
#define REG_PHY			0x7c

static int rtl_phy_wait(struct rtl_priv *priv)
{
	int timeout;

	/* Wait for command bit to be clear */
	for (timeout = 100; timeout > 0; timeout--) {
		if (!(rtl_ioread8(priv, REG_PHY) & 0x80))
			break;
		mdelay(1);
	}
	if (!timeout) {
		msg_err(priv, "PHY register timeout\n");
		return -ETIMEDOUT;
	}
	return 0;
}

static int rtl_phy_write(struct rtl_priv *priv, u8 addr, u32 data)
{
	rtl_phy_wait(priv);
	rtl_iowrite32(priv, REG_PHY, (data << 8) | addr | 0x80);
	rtl_phy_wait(priv);
	if (rtl_ioread8(priv, REG_PHY+2) != (data & 0xff)) {
		msg_err(priv, "PHY write error\n");
		return -EIO;
	}

	return 0;
}

static int rtl_phy_write_cck(struct rtl_priv *priv, u8 addr, u8 data)
{
	return rtl_phy_write(priv, addr, data | 0x10000);
}

static int rtl_phy_write_ofdm(struct rtl_priv *priv, u8 addr, u8 data)
{
	return rtl_phy_write(priv, addr, data);
}

/*
 * Interface with radio frontend
 */

/* Radio frontend interface configuration register */
#define REG_RF_SW_CONFIG	0x08
#define BIT_RF_SW_CONFIG_SI	BIT(1)	/* Serial/Parallel interface */
/* Radio frontend pins */
#define REG_RF_PINS_OUTPUT	0x80
#define REG_RF_PINS_ENABLE	0x82
#define REG_RF_PINS_SELECT	0x84
#define REG_RF_PINS_INPUT	0x86
/* Radio frontend software control register */
#define REG_RF_SW_CTL		0x272
/* Software 3-wire data buffer */
#define REG_SW_3W_DB0		0x274
#define REG_SW_3W_DB1		0x278
/* Software 3-wire control/status registers */
#define REG_SW_3W_CMD0		0x27c
#define BIT_SW_3W_CMD0_HOLD	BIT(7)
#define REG_SW_3W_CMD1		0x27d
#define BIT_SW_3W_CMD1_RE	BIT(0)	/* Read enable */
#define BIT_SW_3W_CMD1_WE	BIT(1)	/* Write enable */
#define BIT_SW_3W_CMD1_DONE	BIT(2)	/* Operation done */
/* Parallel interface data register */
#define REG_PI_DATA_READ	0x360
/* Serial interface data register */
#define REG_SI_DATA_READ	0x362

int rtl_rf_init_io(struct rtl_priv *priv)
{
	/* Power */
	rtl_iowrite16(priv, REG_PI_DATA_READ, 0x1000);
	rtl_iowrite16(priv, REG_SI_DATA_READ, 0x1000);

	/* Override the RFSW_CTRL */
	rtl_iowrite16(priv, REG_RF_SW_CTL, 0x569a);

	/* Setup initial timing for radio frontend */
	rtl_iowrite16(priv, REG_RF_PINS_OUTPUT, 0x0480);
	rtl_iowrite16(priv, REG_RF_PINS_ENABLE, 0x1bff);
	/* Clear BIT(3) to allow HW serial read */
	rtl_iowrite16(priv, REG_RF_PINS_SELECT, 0x2480);

	/* Unknown - initial value of REG_RF_SW_CONFIG */
	/* Select serial interface - set BIT_RF_SW_CONFIG_SI */
	rtl_iowrite8(priv, REG_RF_SW_CONFIG, 0xae | BIT_RF_SW_CONFIG_SI);

	return 0;
}

/* Transfer data to/from RF using hardware 3-wire high speed serial interface */
static int rtl_rf_hssi(struct rtl_priv *priv, u16 *data, bool write)
{
	int timeout;

	/* Wait until RE and WE are cleared */
	for (timeout = 100; timeout > 0; timeout--) {
		u8 reg = rtl_ioread8(priv, REG_SW_3W_CMD1);
		if (!(reg & (BIT_SW_3W_CMD1_WE | BIT_SW_3W_CMD1_RE)))
			break;
		mdelay(1);
	}
	if (!timeout) {
		msg_err(priv, "HSSI wait RE|WE timeout\n");
		return -ETIMEDOUT;
	}

	/* Fill data buffer and set command */
	rtl_iowrite32(priv, REG_SW_3W_DB0, *data);
	rtl_iowrite32(priv, REG_SW_3W_DB1, 0);
	rtl_iowrite8(priv, REG_SW_3W_CMD0, 15);	/* Number of bits - 1 */
	rtl_iowrite8(priv, REG_SW_3W_CMD1, write ? BIT_SW_3W_CMD1_WE : BIT_SW_3W_CMD1_RE);

	/* Wait until operation is done */
	for (timeout = 100; timeout > 0; timeout--) {
		u8 reg = rtl_ioread8(priv, REG_SW_3W_CMD1);
		if (reg & BIT_SW_3W_CMD1_DONE)
			break;
		mdelay(1);
	}
	if (!timeout) {
		msg_err(priv, "HSSI wait DONE timeout\n");
		/* Clear CMD1 here and cancel operation to avoid HSSI full hangup */
		rtl_iowrite8(priv, REG_SW_3W_CMD1, 0);
		return -ETIMEDOUT;
	}

	/* Clear DONE */
	rtl_iowrite8(priv, REG_SW_3W_CMD1, 0);

	/* Read back data */
	if (!write)
		*data = SHIFTOUT(rtl_ioread16(priv, REG_SI_DATA_READ), BITS(0, 11));

	return 0;
}

/* addr is 4-bit, data is 12-bit */
static int rtl_rf_read(struct rtl_priv *priv, u8 addr, u16 *data)
{
	*data = SHIFTOUT(addr, BITS(0, 3));
	return rtl_rf_hssi(priv, data, false);
}

/* addr is 4-bit, data is 12-bit */
static int rtl_rf_write(struct rtl_priv *priv, u8 addr, u16 data)
{
	u16 reg = SHIFTIN(data, BITS(4, 15)) | SHIFTIN(addr, BITS(0, 3));
	return rtl_rf_hssi(priv, &reg, true);
}

/*
 * RF programming code
 */

#define REG_TX_GAIN_CCK		0x9d
#define REG_TX_GAIN_OFDM	0x9e
#define REG_TX_ANTENNA		0x9f

static const u32 rtl_rf_rx_gain_table[] = {
	0x096, 0x076, 0x056, 0x036, 0x016, 0x1f6, 0x1d6, 0x1b6,
	0x196, 0x176, 0x0F7, 0x0D7, 0x0B7, 0x097, 0x077, 0x057,
	0x037, 0x0FB, 0x0DB, 0x0BB, 0x0FF, 0x0E3, 0x0C3, 0x0A3,
	0x083, 0x063, 0x043, 0x023, 0x003, 0x1E3, 0x1C3, 0x1A3,
	0x183, 0x163, 0x143, 0x123, 0x103
};

static const u8 rtl_rf_agc_table[] = {
	0x7E, 0x7E, 0x7E, 0x7E, 0x7D, 0x7C, 0x7B, 0x7A,
	0x79, 0x78, 0x77, 0x76, 0x75, 0x74, 0x73, 0x72,
	0x71, 0x70, 0x6F, 0x6E, 0x6D, 0x6C, 0x6B, 0x6A,
	0x69, 0x68, 0x67, 0x66, 0x65, 0x64, 0x63, 0x62,
	0x48, 0x47, 0x46, 0x45, 0x44, 0x29, 0x28, 0x27,
	0x26, 0x25, 0x24, 0x23, 0x22, 0x21, 0x08, 0x07,
	0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f,
	0x0f, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x15, 0x16,
	0x17, 0x17, 0x18, 0x18, 0x19, 0x1a, 0x1a, 0x1b,
	0x1b, 0x1c, 0x1c, 0x1d, 0x1d, 0x1d, 0x1e, 0x1e,
	0x1f, 0x1f, 0x1f, 0x20, 0x20, 0x20, 0x20, 0x21,
	0x21, 0x21, 0x22, 0x22, 0x22, 0x23, 0x23, 0x24,
	0x24, 0x25, 0x25, 0x25, 0x26, 0x26, 0x27, 0x27,
	0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F
};

static const u8 rtl_rf_ofdm_config[] = {
	/* OFDM reg0x06[7:0]=0xFF: Enable power saving mode in RX */
	/* OFDM reg0x3C[4]=1'b1: Enable RX power saving mode */
	/* ofdm 0x3a = 0x7b, (original: 0xfb) For ECS shielding room TP test */
	0x10, 0x0F, 0x0A, 0x0C, 0x14, 0xFA, 0xFF, 0x50,
	0x00, 0x50, 0x00, 0x00, 0x00, 0x5C, 0x00, 0x00,
	0x40, 0x00, 0x40, 0x00, 0x00, 0x00, 0xA8, 0x26,
	0x32, 0x33, 0x06, 0xA5, 0x6F, 0x55, 0xC8, 0xBB,
	0x0A, 0xE1, 0x2C, 0x4A, 0x86, 0x83, 0x34, 0x00,
	0x4F, 0x24, 0x6F, 0xC2, 0x03, 0x40, 0x80, 0x00,
	0xC0, 0xC1, 0x58, 0xF1, 0x00, 0xC4, 0x90, 0x3e,
	0xD8, 0x3C, 0x7B, 0x10
};

void rtl_rf_start(struct rtl_priv *priv)
{
	u16 reg;
	u32 rfid;
	bool d_cut;
	int i;

	spin_lock(&priv->nic.rf_lock);

	msg_dbg(priv, "Initializing RF\n");

	/* Page 1 */
	rtl_rf_write(priv, 0x0, 0x13f);
	/* Detect RF */
	rtl_rf_read(priv, 0x8, &reg);
	rfid = SHIFTIN(reg, BITS(12, 23));
	rtl_rf_read(priv, 0x9, &reg);
	rfid |= SHIFTIN(reg, BITS(0, 11));
	msg_info(priv, "Radio frontend: %06x\n", rfid);
	d_cut = rfid == 0x81870c;

	/* Page 0 */
	rtl_rf_write(priv, 0x0, 0x09f);
	rtl_rf_write(priv, 0x1, 0x6e0);
	rtl_rf_write(priv, 0x2, 0x04d);
	rtl_rf_write(priv, 0x3, 0x7f1);
	rtl_rf_write(priv, 0x4, 0x975);
	rtl_rf_write(priv, 0x5, 0xc72);
	rtl_rf_write(priv, 0x6, 0xae6);
	rtl_rf_write(priv, 0x7, 0x0ca);
	rtl_rf_write(priv, 0x8, 0xe1c);
	rtl_rf_write(priv, 0x9, 0x2f0);
	rtl_rf_write(priv, 0xa, 0x9d0);
	rtl_rf_write(priv, 0xb, 0x1ba);
	rtl_rf_write(priv, 0xc, 0x640);
	rtl_rf_write(priv, 0xd, 0x8df);
	rtl_rf_write(priv, 0xe, 0x020);
	rtl_rf_write(priv, 0xf, 0x990);

	/* Page 1 */
	rtl_rf_write(priv, 0x0, 0x13f);
	rtl_rf_write(priv, 0x3, 0x806);
	rtl_rf_write(priv, 0x4, 0x3a7);
	rtl_rf_write(priv, 0x5, 0x59b);
	rtl_rf_write(priv, 0x6, 0x081);
	rtl_rf_write(priv, 0x7, 0x1a0);
	rtl_rf_write(priv, 0xa, 0x001);
	rtl_rf_write(priv, 0xb, 0x418);
	rtl_rf_write(priv, 0xc, 0xfbe);
	rtl_rf_write(priv, 0xd, 0x008);
	rtl_rf_write(priv, 0xe, d_cut ? 0x807 : 0x806);	/* RX LO buffer */
	rtl_rf_write(priv, 0xf, 0xacc);
	rtl_rf_write(priv, 0x0, 0x1d7);
	rtl_rf_write(priv, 0x3, 0xe00);
	rtl_rf_write(priv, 0x4, 0xe50);
	for (i = 0; i < ARRAY_SIZE(rtl_rf_rx_gain_table); i++) {
		rtl_rf_write(priv, 0x1, i);
		rtl_rf_write(priv, 0x2, rtl_rf_rx_gain_table[i]);
	}
	rtl_rf_write(priv, 0x5, 0x203);
	rtl_rf_write(priv, 0x6, 0x200);
	rtl_rf_write(priv, 0x0, 0x137); mdelay(10);	/* HSSI disable */
	rtl_rf_write(priv, 0xd, 0x008); mdelay(10);	/* Z4 synthesizer loop filter setting */

	/* Page 0, HSSI disable */
	rtl_rf_write(priv, 0x0, 0x037); mdelay(10);
	rtl_rf_write(priv, 0x4, 0x160); mdelay(10);	/* CBC on, TX/RX disable, high gain */
	rtl_rf_write(priv, 0x7, 0x080); mdelay(10);	/* Z4 set channel 1 */
	rtl_rf_write(priv, 0x2, 0x88d); mdelay(220);	/* LC calibration */

	/* Page 1, HSSI disable */
	rtl_rf_write(priv, 0x0, 0x137); mdelay(10);
	rtl_rf_write(priv, 0x7, 0x000);
	rtl_rf_write(priv, 0x7, 0x180);
	rtl_rf_write(priv, 0x7, 0x220);
	rtl_rf_write(priv, 0x7, 0x3e0);

	/* DAC calibration off */
	rtl_rf_write(priv, 0x6, 0x0c1);
	rtl_rf_write(priv, 0xa, 0x001);

	/* Crystal calibration */
	if (priv->eeprom.xtalcal_enable) {
		msg_info(priv, "Doing crystal calibration: XIN %d XOUT %d\n", priv->eeprom.xtalcal_xin, priv->eeprom.xtalcal_xout);
		rtl_rf_write(priv, 0xf, (priv->eeprom.xtalcal_xin << 5) | (priv->eeprom.xtalcal_xout << 1) | BIT(11) | BIT(9));
	} else
		/* Default XIN = 6, XOUT = 6 */
		rtl_rf_write(priv, 0xf, 0xacc);

	/* Page 0, HSSI enable */
	rtl_rf_write(priv, 0x0, 0x0bf);
	rtl_rf_write(priv, 0xd, 0x8df);	/* RX BB start calibration */
	rtl_rf_write(priv, 0x2, 0x04d); /* Temperature meter off */

	/* RX mode */
	rtl_rf_write(priv, 0x4, 0x975);
	mdelay(30);
	rtl_rf_write(priv, 0x0, 0x197);
	rtl_rf_write(priv, 0x5, 0x5ab);
	rtl_rf_write(priv, 0x0, 0x09f);
	rtl_rf_write(priv, 0x1, 0x000);
	rtl_rf_write(priv, 0x2, 0x000);

	/* CCK configuration */
	rtl_phy_write_cck(priv, 0x00, 0xc8);
	rtl_phy_write_cck(priv, 0x06, 0x1c);
	rtl_phy_write_cck(priv, 0x10, 0x78);
	rtl_phy_write_cck(priv, 0x2e, 0xd0);
	rtl_phy_write_cck(priv, 0x2f, 0x06);
	rtl_phy_write_cck(priv, 0x01, 0x46);

	/* Automatic gain control */
	rtl_phy_write_ofdm(priv, 0x00, 0x12);
	for (i = 0; i < ARRAY_SIZE(rtl_rf_agc_table); i++) {
		rtl_phy_write_ofdm(priv, 0x0f, rtl_rf_agc_table[i]);
		rtl_phy_write_ofdm(priv, 0x0e, i+0x80);
		rtl_phy_write_ofdm(priv, 0x0e, 0);
	}
	rtl_phy_write_ofdm(priv, 0x00, 0x10);

	/* OFDM configuration */
	for (i = 0; i < ARRAY_SIZE(rtl_rf_ofdm_config); i++)
		rtl_phy_write_ofdm(priv, i, rtl_rf_ofdm_config[i]);

	spin_unlock(&priv->nic.rf_lock);
}

static void rtl_rf_turn_on(struct rtl_priv *priv)
{
	/* Enable writing analog parameters */
	rtl_unlock_anaparam(priv);

	/* Turn on AFE */
	rtl_iowrite16(priv, 0x370, 0x0560);
	rtl_iowrite16(priv, 0x372, 0x0560);
	rtl_iowrite16(priv, 0x374, 0x0da4);
	rtl_iowrite16(priv, 0x376, 0x0da4);
	rtl_iowrite16(priv, 0x378, 0x0560);
	rtl_iowrite16(priv, 0x37a, 0x0560);
	rtl_iowrite16(priv, 0x37c, 0x00ec);
	rtl_iowrite16(priv, 0x37e, 0x00ec);
	rtl_iowrite32(priv, 0x54, 0xb0054d00);
	rtl_iowrite32(priv, 0x60, 0x000004c6);
	rtl_iowrite16(priv, 0xee, 0x0010);

	/* Turn on RF */
	rtl_rf_write(priv, 0x0, 0x09f); udelay(500);
	rtl_rf_write(priv, 0x4, 0x975 /* 0x972 */); udelay(500);
	// TODO: rtl_rf_start writes 0x975 to 0x4, but we write 0x972 - manage with it

	/* Turn on BB */
	rtl_phy_write_ofdm(priv, 0x10, 0x40);
	rtl_phy_write_ofdm(priv, 0x12, 0x40);
	rtl_iowrite8(priv, 0x24e, 0x01);

	/* Disable writing analog parameters */
	rtl_lock_anaparam(priv);
}

static void rtl_rf_turn_off(struct rtl_priv *priv)
{
	int timeout;

	/* Enable writing analog parameters */
	rtl_unlock_anaparam(priv);

	/* Turn off BB RXIQ matrix to cut off RX signal */
	rtl_phy_write_ofdm(priv, 0x10, 0x00);
	rtl_phy_write_ofdm(priv, 0x12, 0x00);

	/* Turn off RF */
	rtl_rf_write(priv, 0x4, 0x01f /* 0x000 */);
	rtl_rf_write(priv, 0x0, 0x000);
	// TODO: rtl_rf_start writes 0x01f to 0x4, but we write 0x000 - manage with it

	/* Turn off AFE except PLL */
	rtl_iowrite32(priv, 0x54, 0x554806ec);
	rtl_iowrite32(priv, 0x60, 0x72ff3f70);
	rtl_iowrite16(priv, 0xee, 0x0000);

	mdelay(1);

	for (timeout = 100; timeout > 0; timeout--) {
		u8 reg = rtl_ioread8(priv, 0x24f);
		if (reg == 0x01 || reg == 0x09)
			break;
		udelay(10);
	}
	if (!timeout) {
		msg_warn(priv, "BB polling timeout\n");

		/* Disable writing analog parameters */
		rtl_lock_anaparam(priv);
		return;
	}

	/* Turn off BB */
	rtl_iowrite8(priv, 0x24e, 0x61);

	/* Turn off AFE PLL */
	rtl_iowrite8(priv, 0x54, 0xfc);
	rtl_iowrite16(priv, 0x37c, 0x00fc);

	/* Disable writing analog parameters */
	rtl_lock_anaparam(priv);
}

bool rtl_rf_set_power_state(struct rtl_priv *priv, bool state)
{
	spin_lock(&priv->nic.rf_lock);

	msg_info(priv, "Setting RF power state: %s\n", state ? "on" : "off");

	if (priv->nic.rf_power == state) {
		spin_unlock(&priv->nic.rf_lock);
		return false;
	}

	if (state)
		rtl_rf_turn_on(priv);
	else
		rtl_rf_turn_off(priv);

	priv->nic.rf_power = state;

	spin_unlock(&priv->nic.rf_lock);

	return true;
}

static void rtl_rf_lock(struct rtl_priv *priv)
{
	spin_lock(&priv->nic.rf_lock);
	priv->nic.rf_power_save = priv->nic.rf_power;
	/* Turn RF on to avoid PHY register write problems */
	if (!priv->nic.rf_power_save)
		rtl_rf_turn_on(priv);
}

static void rtl_rf_unlock(struct rtl_priv *priv)
{
	/* Restore original RF power state */
	if (!priv->nic.rf_power_save)
		rtl_rf_turn_off(priv);
	spin_unlock(&priv->nic.rf_lock);
}

static void rtl_rf_set_tx_power(struct rtl_priv *priv, int channel)
{
	u8 cck_power = rtl_rf_cck_power(priv, channel);
	u8 ofdm_power = rtl_rf_ofdm_power(priv, channel);

	if (priv->dm.lowered_tx_pwr) {
		cck_power = cck_power > 16 ? cck_power-16 : 0;
		ofdm_power = ofdm_power > 16 ? ofdm_power-16 : 0;
	}

	msg_dbg(priv, "Setting TX power CCK %d OFDM %d\n", cck_power, ofdm_power);

	rtl_iowrite8(priv, REG_TX_GAIN_CCK, cck_power);
	mdelay(1);
	rtl_iowrite8(priv, REG_TX_GAIN_OFDM, ofdm_power);

	if (ofdm_power <= 11) {
		rtl_phy_write_ofdm(priv, 0x07, 0x5c);
		rtl_phy_write_ofdm(priv, 0x09, 0x5c);
	} else if (ofdm_power <= 17) {
		rtl_phy_write_ofdm(priv, 0x07, 0x54);
		rtl_phy_write_ofdm(priv, 0x09, 0x54);
	} else {
		rtl_phy_write_ofdm(priv, 0x07, 0x50);
		rtl_phy_write_ofdm(priv, 0x09, 0x50);
	}

	mdelay(1);
}

static const u16 rtl_rf_channel[] = {
	0x080, 0x100, 0x180, 0x200, 0x280, 0x300, 0x380,
	0x400, 0x480, 0x500, 0x580, 0x600, 0x680, 0x74A,
};

void rtl_rf_set_channel(struct rtl_priv *priv, int channel)
{
	rtl_rf_lock(priv);

	msg_dbg(priv, "Setting channel %d\n", channel);

	rtl_rf_set_tx_power(priv, channel);
	rtl_rf_write(priv, 0x7, rtl_rf_channel[channel-1]);
	mdelay(10);

	rtl_rf_unlock(priv);
}

void rtl_rf_set_antenna(struct rtl_priv *priv, enum rtl_antenna antenna, bool diversity)
{
	bool main_antenna = antenna == RTL_ANTENNA_MAIN;

	rtl_rf_lock(priv);

	msg_info(priv, "Switching to %s antenna\n", main_antenna ? "main" : "aux");

	priv->dm.antenna = antenna;

	/* Threshold for antenna diversity */
	rtl_phy_write_cck(priv, 0x0c, 0x09);

	/* Antenna select */
	rtl_iowrite8(priv, REG_TX_ANTENNA, main_antenna ? 0x03 : 0x00);

	/* Config CCK RX antenna */
	rtl_phy_write_cck(priv, 0x11, main_antenna ? 0x9b : 0xbb);
	rtl_phy_write_cck(priv, 0x01, diversity ? 0xc7 : 0x47);

	/* Config OFDM RX antenna */
	rtl_phy_write_ofdm(priv, 0x0d, main_antenna ? 0x5c : 0x54);
	rtl_phy_write_ofdm(priv, 0x18, diversity ? 0xb2 : 0x32);

	rtl_rf_unlock(priv);
}

void rtl_rf_set_initial_gain(struct rtl_priv *priv, int gain)
{
	u8 values[][3] = {
		{ 0x26, 0x86, 0xfa },
		{ 0x36, 0x86, 0xfa },
		{ 0x36, 0x86, 0xfb },
		{ 0x46, 0x86, 0xfb },
		{ 0x46, 0x96, 0xfb },
		{ 0x56, 0x96, 0xfc },
		{ 0x56, 0xa6, 0xfc },
		{ 0x66, 0xb6, 0xfc },
	};
	int idx = clamp_t(int, gain, 1, ARRAY_SIZE(values)) - 1;

	rtl_rf_lock(priv);

	msg_dbg(priv, "Setting initial gain %d\n", gain);

	priv->dm.initial_gain = gain;

	rtl_phy_write_ofdm(priv, 0x17, values[idx][0]); mdelay(1);
	rtl_phy_write_ofdm(priv, 0x24, values[idx][1]); mdelay(1);
	rtl_phy_write_ofdm(priv, 0x05, values[idx][2]); mdelay(1);

	rtl_rf_unlock(priv);
}

void rtl_rf_set_txpwr_tracking(struct rtl_priv *priv, bool state)
{
	u16 reg;

	rtl_rf_lock(priv);

	rtl_rf_read(priv, 0x2, &reg);
	if (state)
		reg |= BIT(1);
	else
		reg &= ~BIT(1);
	rtl_rf_write(priv, 0x2, reg);

	rtl_rf_unlock(priv);
}
