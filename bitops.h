#pragma once

#include <linux/bitops.h>

#define LOWER_BITS(nr)			(((nr) >= BITS_PER_LONG ? 0 : BIT(nr)) - 1)
#define BITS(from, to)			(LOWER_BITS((to) + 1) ^ LOWER_BITS(from))
#define LOWEST_SET_BIT(mask)		((((mask) - 1) & (mask)) ^ (mask))
#define SHIFTOUT(x, mask)		(((x) & (mask)) / LOWEST_SET_BIT(mask))
#define SHIFTIN(x, mask)		(((x) * LOWEST_SET_BIT(mask)) & (mask))
