#pragma once

#include "global.h"

/*
 * Dynamic Management
 */

void rtl_dm_watchdog(struct rtl_priv *priv);
void rtl_dm_antenna_diversity(struct rtl_priv *priv);
