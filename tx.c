#include "tx.h"
#include "desc.h"
#include "hw.h"

static u8 rtl_mac_to_hwqueue(struct sk_buff *skb)
{
	static const u8 ac_to_hwq[] = { 4, 3, 2, 1 };
	__le16 fc = ((struct ieee80211_hdr *)(skb->data))->frame_control;
	u8 queue_index = skb_get_queue_mapping(skb);

	if (unlikely(ieee80211_is_beacon(fc)))
		return RTL_TX_QUEUE_BEACON;
	if (ieee80211_is_mgmt(fc))
		return 0;
	if (ieee80211_is_nullfunc(fc))
		return 5;

	return ac_to_hwq[queue_index];
}

static void rtl_tx(struct rtl_priv *priv, struct sk_buff *skb, u8 hw_queue)
{
	struct ieee80211_hw *hw = priv->hw;
	struct ieee80211_tx_info *info = IEEE80211_SKB_CB(skb);
	struct rtl_tx_ring *ring = &priv->tx[hw_queue];
	struct ieee80211_hdr *hdr = (struct ieee80211_hdr *)(skb->data);
	bool firstseg = !(hdr->seq_ctrl & cpu_to_le16(IEEE80211_SCTL_FRAG));
	bool lastseg = !(hdr->frame_control & cpu_to_le16(IEEE80211_FCTL_MOREFRAGS));
	dma_addr_t mapping = pci_map_single(priv->pdev, skb->data, skb->len, PCI_DMA_TODEVICE);
	u8 rc_flags = info->control.rates[0].flags;
	struct ieee80211_rate *txrate = ieee80211_get_tx_rate(hw, info);
	u8 idx = (hw_queue != RTL_TX_QUEUE_BEACON) ? (ring->idx + skb_queue_len(&ring->queue)) % ring->count : 0;
	struct rtl_tx_desc *desc = &ring->desc[idx];
	u32 flags = le32_to_cpu(desc->flags);

	if (unlikely((flags & BIT_TX_DESC_FLAG_OWN) && hw_queue != RTL_TX_QUEUE_BEACON)) {
		msg_err(priv, "No more TX desc in queue %d idx %d:%d\n", hw_queue, ring->idx, idx);
		dev_kfree_skb_any(skb);
		return;
	}

	msg_dbg(priv, "TXing frame queue %d idx %d:%d\n", hw_queue, ring->idx, idx);

	if (ieee80211_is_nullfunc(hdr->frame_control))
		msg_dbg(priv, "TXing nullfunc PM=%d\n", ieee80211_has_pm(hdr->frame_control));

	/* Fill TX descriptor */
	desc->rts_duration = 0;
	flags = SHIFTIN(skb->len, MASK_TX_DESC_BUF_SIZE) |
		SHIFTIN(txrate->hw_value, MASK_TX_DESC_RATE_IDX);
	flags |= BIT_TX_DESC_FLAG_DMA | BIT_TX_DESC_FLAG_NO_ENC;
	if (firstseg)
		flags |= BIT_TX_DESC_FLAG_FS;
	if (lastseg)
		flags |= BIT_TX_DESC_FLAG_LS;
	if (ieee80211_has_morefrags(hdr->frame_control))
		flags |= BIT_TX_DESC_FLAG_MOREFRAG;
	if (rc_flags & IEEE80211_TX_RC_USE_RTS_CTS) {
		flags |= BIT_TX_DESC_FLAG_RTS;
		flags |= SHIFTIN(ieee80211_get_rts_cts_rate(hw, info)->hw_value, MASK_TX_DESC_RTS_RATE_IDX);
		desc->rts_duration = ieee80211_rts_duration(hw, priv->vif, skb->len, info);
	} else if (rc_flags & IEEE80211_TX_RC_USE_CTS_PROTECT) {
		flags |= BIT_TX_DESC_FLAG_CTS;
		flags |= SHIFTIN(ieee80211_get_rts_cts_rate(hw, info)->hw_value, MASK_TX_DESC_RTS_RATE_IDX);
	}
	if (rc_flags & IEEE80211_TX_RC_USE_SHORT_PREAMBLE)
		flags |= BIT_TX_DESC_FLAG_SPLCP;
	desc->flags = cpu_to_le32(flags);
	desc->tx_buf = cpu_to_le32(mapping);
	desc->frame_len = cpu_to_le16(skb->len & 0xfff);
	desc->tx_duration = ieee80211_generic_frame_duration(hw, priv->vif, info->band, skb->len, txrate);
	desc->retry = cpu_to_le32((info->control.rates[0].count - 1) << 8);

	if (info->flags & IEEE80211_TX_CTL_ASSIGN_SEQ) {
		if (info->flags & IEEE80211_TX_CTL_FIRST_FRAGMENT)
			priv->nic.seqno++;
		hdr->seq_ctrl &= ~cpu_to_le16(IEEE80211_SCTL_SEQ);
		hdr->seq_ctrl |= cpu_to_le16(SHIFTIN(priv->nic.seqno, IEEE80211_SCTL_SEQ));
	}

	// TODO: is that really needed?
//	if (priv->ps.lps_active && ieee80211_is_data(hdr->frame_control) && !ieee80211_is_nullfunc(hdr->frame_control) && !ieee80211_has_pm(hdr->frame_control))
//		hdr->frame_control |= cpu_to_le16(IEEE80211_FCTL_PM);

	__skb_queue_tail(&ring->queue, skb);

	desc->flags |= cpu_to_le32(BIT_TX_DESC_FLAG_OWN);

	if (hw_queue != RTL_TX_QUEUE_BEACON && ring->count - skb_queue_len(&ring->queue) < 2)
		ieee80211_stop_queue(hw, skb_get_queue_mapping(skb));

	priv->dm.rate_idx = txrate->hw_value;

	rtl_tx_dma(priv, hw_queue);
}

void rtl_op_tx(struct ieee80211_hw *hw, struct ieee80211_tx_control *control, struct sk_buff *skb)
{
	RTL_PRIV(hw);
	u8 hw_queue = rtl_mac_to_hwqueue(skb);
	unsigned long cpu_flags;

	spin_lock_irqsave(&priv->tx[hw_queue].lock, cpu_flags);
	rtl_tx(priv, skb, hw_queue);
	spin_unlock_irqrestore(&priv->tx[hw_queue].lock, cpu_flags);
}

void rtl_op_flush(struct ieee80211_hw *hw, bool drop)
{
	RTL_PRIV(hw);
	u8 queue;

	for (queue = 0; queue < RTL_TX_QUEUE_BEACON; queue++)
		rtl_flush(priv, queue, 20);
}

void rtl_tx_beacon(struct rtl_priv *priv)
{
	struct rtl_tx_ring *ring = &priv->tx[RTL_TX_QUEUE_BEACON];
	struct rtl_tx_desc *desc = &ring->desc[0];
	struct sk_buff *skb;
	unsigned long cpu_flags;

	spin_lock_irqsave(&ring->lock, cpu_flags);

	skb = __skb_dequeue(&ring->queue);
	if (skb) {
		pci_unmap_single(priv->pdev, le32_to_cpu(desc->tx_buf), skb->len, PCI_DMA_TODEVICE);
		dev_kfree_skb(skb);
	}

	skb = ieee80211_beacon_get(priv->hw, priv->vif);
	if (skb)
		rtl_tx(priv, skb, RTL_TX_QUEUE_BEACON);

	spin_unlock_irqrestore(&ring->lock, cpu_flags);
}

void rtl_isr_tx(struct rtl_priv *priv, int priority)
{
	struct rtl_tx_ring *ring = &priv->tx[priority];
	size_t cnt = 0;
	size_t len;
	size_t idx;

	spin_lock(&ring->lock);

	len = skb_queue_len(&ring->queue);
	idx = (priority != RTL_TX_QUEUE_BEACON) ? (ring->idx + len) % ring->count : 0;

	msg_dbg(priv, "TX ISR priority %d len %u idx %d:%d:%d\n", priority, len, ring->idx, rtl_get_tx_ring_pointer(priv, priority), idx);

	while (!skb_queue_empty(&ring->queue)) {
		struct rtl_tx_desc *desc = &ring->desc[ring->idx];
		struct sk_buff *skb;
		struct ieee80211_tx_info *info;
		struct ieee80211_hdr *hdr;
		u32 flags = le32_to_cpu(desc->flags);

		if (flags & BIT_TX_DESC_FLAG_OWN) {
			msg_dbg(priv, "TX ISR priority %d idx %d:%d: TXed %u packets, len %u\n", priority, ring->idx, idx, cnt, skb_queue_len(&ring->queue));
			spin_unlock(&ring->lock);
			return;
		}

		cnt++;

		ring->idx = (ring->idx + 1) % ring->count;

		skb = __skb_dequeue(&ring->queue);
		pci_unmap_single(priv->pdev, le32_to_cpu(desc->tx_buf), skb->len, PCI_DMA_TODEVICE);

		hdr = (struct ieee80211_hdr *)(skb->data);
		if (ieee80211_is_nullfunc(hdr->frame_control))
			msg_dbg(priv, "TXed nullfunc PM=%d\n", ieee80211_has_pm(hdr->frame_control));

		info = IEEE80211_SKB_CB(skb);
		ieee80211_tx_info_clear_status(info);

		if (!(info->flags & IEEE80211_TX_CTL_NO_ACK) && (flags & BIT_TX_DESC_FLAG_TX_OK))
			info->flags |= IEEE80211_TX_STAT_ACK;

		info->status.rates[0].count = (flags & 0xff) + 1;
		info->status.rates[1].idx = -1;

		ieee80211_tx_status_irqsafe(priv->hw, skb);

		if ((ring->count - skb_queue_len(&ring->queue)) == 2)
			ieee80211_wake_queue(priv->hw, skb_get_queue_mapping(skb));
	}

	msg_dbg(priv, "TX ISR priority %d idx %d:%d: TXed %u packets, queue empty\n", priority, ring->idx, idx, cnt);
	spin_unlock(&ring->lock);
}
