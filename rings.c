#include "rings.h"
#include "desc.h"

static void rtl_free_rx_ring(struct rtl_priv *priv, size_t queue)
{
	struct rtl_rx_ring *ring = &priv->rx[queue];
	size_t ring_size = sizeof(*ring->desc) * RTL_RX_BUF_COUNT;
	size_t i;

	for (i = 0; i < RTL_RX_BUF_COUNT; i++) {
		struct sk_buff *skb = ring->buf[i];
		if (!skb)
			continue;
		pci_unmap_single(priv->pdev, *((dma_addr_t *)skb->cb), RTL_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);
		dev_kfree_skb(skb);
	}

	if (ring->desc) {
		pci_free_consistent(priv->pdev, ring_size, priv->rx[queue].desc, priv->rx[queue].dma);
		ring->desc = NULL;
	}
}

static int rtl_init_rx_ring(struct rtl_priv *priv, size_t queue)
{
	struct rtl_rx_ring *ring = &priv->rx[queue];
	size_t ring_size = sizeof(*ring->desc) * RTL_RX_BUF_COUNT;
	size_t i;

	ring->desc = pci_alloc_consistent(priv->pdev, ring_size, &ring->dma);
	if (!ring->desc || (uintptr_t)ring->desc & 0xff) {
		msg_err(priv, "Cannot allocate RX ring %u\n", queue);
		return -ENOMEM;
	}

	memset(ring->desc, 0, ring_size);
	ring->idx = 0;

	/* Fill ring->buf[] by NULLs so that we could call rtl_free_rx_ring()
	 * safely in case of skb allocation failure
	 */
	memset(ring->buf, 0, sizeof(ring->buf));

	for (i = 0; i < RTL_RX_BUF_COUNT; i++) {
		dma_addr_t *dma;
		struct sk_buff *skb = dev_alloc_skb(RTL_RX_BUF_SIZE);
		if (!skb) {
			msg_err(priv, "Unable to alloc RX skb\n");
			rtl_free_rx_ring(priv, queue);
			return -ENOMEM;
		}

		ring->buf[i] = skb;
		dma = (dma_addr_t *)skb->cb;
		*dma = pci_map_single(priv->pdev, skb_tail_pointer(skb), RTL_RX_BUF_SIZE, PCI_DMA_FROMDEVICE);
		ring->desc[i].rx_buf = cpu_to_le32(*dma);
		ring->desc[i].flags = cpu_to_le32(BIT_RX_DESC_FLAG_OWN | SHIFTIN(RTL_RX_BUF_SIZE, MASK_RX_DESC_BUF_SIZE));
	}
	ring->desc[RTL_RX_BUF_COUNT-1].flags |= cpu_to_le32(BIT_RX_DESC_FLAG_EOR);

	return 0;
}

static void rtl_free_tx_ring(struct rtl_priv *priv, size_t queue)
{
	struct rtl_tx_ring *ring = &priv->tx[queue];
	size_t ring_size = sizeof(*ring->desc) * ring->count;

	spin_lock(&ring->lock);

	while (!skb_queue_empty(&ring->queue)) {
		struct sk_buff *skb = __skb_dequeue(&ring->queue);
		struct rtl_tx_desc *entry = &ring->desc[ring->idx];

		pci_unmap_single(priv->pdev, le32_to_cpu(entry->tx_buf), skb->len, PCI_DMA_TODEVICE);
		dev_kfree_skb(skb);
		ring->idx = (ring->idx+1) % ring->count;
	}

	if (ring->desc) {
		pci_free_consistent(priv->pdev, ring_size, ring->desc, ring->dma);
		ring->desc = NULL;
	}

	spin_unlock(&ring->lock);
}

static int rtl_init_tx_ring(struct rtl_priv *priv, size_t queue, size_t count)
{
	struct rtl_tx_ring *ring = &priv->tx[queue];
	size_t ring_size = sizeof(*ring->desc) * count;
	size_t i;

	ring->desc = pci_alloc_consistent(priv->pdev, ring_size, &ring->dma);
	if (!ring->desc || (uintptr_t)ring->desc & 0xff) {
		msg_err(priv, "Cannot allocate TX ring %u\n", queue);
		return -ENOMEM;
	}

	memset(ring->desc, 0, ring_size);
	ring->idx = 0;
	ring->count = count;
	skb_queue_head_init(&ring->queue);
	spin_lock_init(&ring->lock);

	for (i = 0; i < count; i++)
		ring->desc[i].next_tx_desc = cpu_to_le32(ring->dma + ((i+1) % count) * sizeof(ring->desc[i]));

	return 0;
}

int rtl_init_rings(struct rtl_priv *priv)
{
	int err;
	size_t i;

	err = rtl_init_rx_ring(priv, 0);
	if (err)
		return err;

	for (i = 0; i < RTL_TX_QUEUE_COUNT; i++) {
		err = rtl_init_tx_ring(priv, i, i == RTL_TX_QUEUE_BEACON ? 2 : 256);
		if (err)
			goto rtl_init_rings_free;
	}

	return 0;

rtl_init_rings_free:
	rtl_free_rx_ring(priv, 0);
	for (i--; i >= 0; i--)
		rtl_free_tx_ring(priv, i);

	return err;
}

void rtl_deinit_rings(struct rtl_priv *priv)
{
	size_t i;

	rtl_free_rx_ring(priv, 0);
	for (i = 0; i < RTL_TX_QUEUE_COUNT; i++)
		rtl_free_tx_ring(priv, i);
}
