#pragma once

#include "global.h"
#include "bitops.h"

/*
 * Radio Frontend
 */

int rtl_rf_init_io(struct rtl_priv *priv);
void rtl_rf_start(struct rtl_priv *priv);
bool rtl_rf_set_power_state(struct rtl_priv *priv, bool state);
void rtl_rf_set_channel(struct rtl_priv *priv, int channel);
void rtl_rf_set_antenna(struct rtl_priv *priv, enum rtl_antenna antenna, bool diversity);
void rtl_rf_set_initial_gain(struct rtl_priv *priv, int gain);
void rtl_rf_set_txpwr_tracking(struct rtl_priv *priv, bool state);

static inline u8 rtl_rf_cck_power(struct rtl_priv *priv, int channel)
{
	return min(SHIFTOUT(priv->channels[channel-1].hw_value, (u16)BITS(0, 7)), 35);
}

static inline u8 rtl_rf_ofdm_power(struct rtl_priv *priv, int channel)
{
	return min(SHIFTOUT(priv->channels[channel-1].hw_value, (u16)BITS(8, 15)), 35);
}
