#pragma once

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/module.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/mutex.h>
#include <net/mac80211.h>

#define RTL_RX_QUEUE_COUNT	1
#define RTL_RX_BUF_COUNT	64
#define RTL_RX_BUF_SIZE		IEEE80211_MAX_RTS_THRESHOLD
#define RTL_TX_QUEUE_COUNT	7
#define RTL_TX_QUEUE_BEACON	6
#define RTL_WATCHDOG_INTERVAL	(1 * HZ)

#define RTL_PRIV(hw)			struct rtl_priv * const priv = ((struct ieee80211_hw *)(hw))->priv
#define msg_err(p, fmt, args...)	wiphy_err((p)->hw->wiphy, fmt, ##args)
#define msg_warn(p, fmt, args...)	wiphy_warn((p)->hw->wiphy, fmt, ##args)
#define msg_info(p, fmt, args...)	wiphy_info((p)->hw->wiphy, fmt, ##args)
#define msg_dbg(p, fmt, args...)	wiphy_dbg((p)->hw->wiphy, fmt, ##args)

struct rtl_rx_desc {
	__le32 flags;
	union {
		__le64 mac_time;
		struct {
			u32 reserved1;
			__le32 rx_buf;
		} __packed;
	};
	u8 sq;
	u8 rssi;
	u8 agc;
	u8 flags2;
	u8 unknown1;
	u8 unknown2;
	u8 rxpower;
	u8 fot;
	u32 reserved2[3];
} __packed;

struct rtl_tx_desc {
	__le32 flags;
	__le16 rts_duration;
	__le16 len;
	__le32 tx_buf;
	__le16 frame_len;
	__le16 tx_duration;
	__le32 next_tx_desc;
	__le32 retry;
	u32 reserved[2];
} __packed;

struct rtl_rx_ring {
	struct rtl_rx_desc *desc;
	dma_addr_t dma;
	size_t idx;
	struct sk_buff *buf[RTL_RX_BUF_COUNT];
};

struct rtl_tx_ring {
	struct rtl_tx_desc *desc;
	dma_addr_t dma;
	size_t idx;
	size_t count;
	struct sk_buff_head queue;
	spinlock_t lock;
};

struct rtl_eeprom {
	u8 country_code;

	bool antenna_diversity;
	bool default_antenna;	/* true if first, false if second */

	bool xtalcal_enable;
	u8 xtalcal_xout;
	u8 xtalcal_xin;

	bool txpwr_tracking;
	u8 thermal_meter;
};

struct rtl_nic {
	u16 seqno;
	u32 rx_conf;
	bool irq_enabled;
	bool beacon_enabled;
	bool rf_power;
	bool rf_power_save;
	spinlock_t rf_lock;
};

struct rtl_ps {
	spinlock_t ps_lock;
	bool ips_active; 	/* Inactive Power Saving */
	bool lps_active;	/* Software Leisure Power Saving */
};

enum rtl_antenna {
	RTL_ANTENNA_AUX,
	RTL_ANTENNA_MAIN,

	RTL_ANTENNA_CNT,
	RTL_ANTENNA_MAX = RTL_ANTENNA_CNT-1
};

struct rtl_dm {
	/* High power mechanism */
	int smoothed_signal_strength;
	bool last_cck;
	u8 last_cck_rssi;
	bool lowered_tx_pwr;
	int rate_idx;

	/* Dynamic initial gain */
	int initial_gain;
	int fallback_votes;
	int upgrade_votes;

	/* TX power tracking */
	u8 thermal_meter;

	/* Antenna diversity */
	enum rtl_antenna antenna;
	int rx_ok_cnt;
	int rx_ok_cnt_antenna[RTL_ANTENNA_CNT];
	/* RX signal strength */
	int ad_rx_ss;
	bool ad_switched_checking;
	/* Signal strength threshold to switch antenna */
	int ad_rx_ss_threshold;
	/* Maximal value of ad_rx_ss_threshold */
	int ad_max_rx_ss_threshold;
	/* Signal strength before switched antenna */
	int ad_rx_ss_before_switched;
};

struct rtl_wdt {
	struct delayed_work work;
	bool running;
	u32 ticks;
};

struct rtl_priv {
	struct pci_dev *pdev;
	struct ieee80211_hw *hw;
	struct ieee80211_vif *vif;

	void __iomem *iomem;

	struct ieee80211_channel channels[14];
	struct ieee80211_rate bitrates[12];
	struct ieee80211_supported_band band;
	struct ieee80211_tx_queue_params qos_params[IEEE80211_NUM_ACS];

	struct rtl_rx_ring rx[RTL_RX_QUEUE_COUNT];
	struct rtl_tx_ring tx[RTL_TX_QUEUE_COUNT];

	struct rtl_eeprom eeprom;
	struct rtl_nic nic;
	struct rtl_ps ps;
	struct rtl_dm dm;
	struct rtl_wdt wdt;

	bool rfkill_state;
};

struct rtl_vif {
	struct ieee80211_hw *hw;
	struct tasklet_struct beacon_tasklet;
};
