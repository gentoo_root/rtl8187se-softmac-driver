#include "dm.h"
#include "io.h"
#include "reg.h"
#include "rf.h"
#include "main.h"

/* CCK and OFDM false alarm counters */
#define REG_CCK_FALSE_ALARM	0xd0
#define REG_OFDM_FALSE_ALARM	0xd2

static bool dm_enabled = true;
module_param(dm_enabled, bool, S_IRUGO);

static bool tx_high_power_enabled = true;
module_param(tx_high_power_enabled, bool, S_IRUGO);

static bool dig_enabled = true;
module_param(dig_enabled, bool, S_IRUGO);

static bool txpwr_tracking_enabled = true;
module_param(txpwr_tracking_enabled, bool, S_IRUGO);

static bool antenna_diversity_enabled = true;
module_param(antenna_diversity_enabled, bool, S_IRUGO);

static void rtl_dm_tx_high_power(struct rtl_priv *priv)
{
	if (priv->dm.smoothed_signal_strength > 770 || (priv->dm.last_cck && priv->dm.last_cck_rssi > 70)) {
		if (!priv->dm.lowered_tx_pwr) {
			priv->dm.lowered_tx_pwr = true;
			msg_dbg(priv, "Lowering TX power by 8 dBm\n");
			rtl_update_channel(priv);
		}
	} else
	if (priv->dm.smoothed_signal_strength < 750 && (!priv->dm.last_cck || priv->dm.last_cck_rssi < 20)) {
		if (priv->dm.lowered_tx_pwr) {
			priv->dm.lowered_tx_pwr = false;
			msg_dbg(priv, "Restoring TX power\n");
			rtl_update_channel(priv);
		}
	}
}

static void rtl_dm_dig(struct rtl_priv *priv)
{
	u16 ofdm_false_alarm = rtl_ioread16(priv, REG_OFDM_FALSE_ALARM);

	msg_dbg(priv, "DIG: OFDM false alarm %#06x\n", ofdm_false_alarm);

	if (ofdm_false_alarm > 0x0c00) {
		priv->dm.upgrade_votes = 0;
		if (++priv->dm.fallback_votes > 1) {
			/* Serious OFDM false alarm, need fallback */
			if (priv->dm.initial_gain < 8)
				rtl_rf_set_initial_gain(priv, priv->dm.initial_gain+1);
			priv->dm.fallback_votes = 0;
		}
	} else
	if (ofdm_false_alarm <= 0x0020) {
		priv->dm.fallback_votes = 0;
		if (++priv->dm.upgrade_votes > 9) {
			if (priv->dm.initial_gain > 1)
				rtl_rf_set_initial_gain(priv, priv->dm.initial_gain-1);
			priv->dm.upgrade_votes = 0;
		}
	} else {
		priv->dm.fallback_votes = 0;
		priv->dm.upgrade_votes = 0;
	}
}

static void rtl_dm_txpwr_tracking(struct rtl_priv *priv)
{
	/* Enable LPF Calibration Register */
	u8 thermal_meter = min(SHIFTOUT(rtl_ioread8(priv, 0x238), (u8)BITS(4, 7)), 0x0c);

	msg_dbg(priv, "TX power tracking: thermal_meter=%d, was %d\n", thermal_meter, priv->dm.thermal_meter);

	if (priv->dm.thermal_meter != thermal_meter) {
		s8 thermal_delta = (thermal_meter - priv->dm.thermal_meter) * 2;
		int i;

		for (i = 1; i <= 14; i++) {
			u8 cck_power  = rtl_rf_cck_power(priv, i);
			u8 ofdm_power = rtl_rf_ofdm_power(priv, i);
			msg_dbg(priv, "Channel %d: was CCK %d OFDM %d\n", i, cck_power, ofdm_power);
			cck_power = clamp_t(s8, cck_power+thermal_delta, 0, 35);
			ofdm_power = clamp_t(s8, ofdm_power+thermal_delta, 0, 35);
			msg_dbg(priv, "Channel %d: now CCK %d OFDM %d\n", i, cck_power, ofdm_power);
			priv->channels[i].hw_value = SHIFTIN(cck_power, BITS(0, 7)) | SHIFTIN(ofdm_power, BITS(8, 16));
		}

		priv->dm.thermal_meter = thermal_meter;

		rtl_update_channel(priv);
	}
}

void rtl_dm_watchdog(struct rtl_priv *priv)
{
	if (!dm_enabled)
		return;

	if (tx_high_power_enabled)
		rtl_dm_tx_high_power(priv);

	msg_dbg(priv, "DIG: rate %d\n", priv->dm.rate_idx);

	if (dig_enabled && priv->vif && priv->vif->bss_conf.assoc && priv->dm.rate_idx >= 7)
		rtl_dm_dig(priv);

	if (txpwr_tracking_enabled && priv->eeprom.txpwr_tracking && !priv->dm.lowered_tx_pwr)
		rtl_dm_txpwr_tracking(priv);
}

void rtl_dm_antenna_diversity(struct rtl_priv *priv)
{
	bool linked = priv->vif && priv->vif->bss_conf.assoc;
	enum rtl_antenna new_antenna =
		priv->dm.antenna == RTL_ANTENNA_MAIN ?
		RTL_ANTENNA_AUX : RTL_ANTENNA_MAIN;

	if (!dm_enabled)
		return;
	if (!antenna_diversity_enabled)
		return;
	if (!priv->eeprom.antenna_diversity)
		return;

	msg_dbg(priv, "Antenna diversity callback\n");

	if (!linked) {
		priv->dm.ad_switched_checking = false;
		/* Switch antenna to prevent any of antenna is broken before
		 * link established */
		rtl_rf_set_antenna(priv, new_antenna, true);
	} else if (priv->dm.rx_ok_cnt == 0) {
		priv->dm.ad_switched_checking = false;
		/* Linked but no packet received */
		rtl_rf_set_antenna(priv, new_antenna, true);
	} else if (priv->dm.ad_switched_checking) {
		/* Evaluate last switch action and undo it if necessary */
		priv->dm.ad_switched_checking = false;

		/* Adjust RX signal strength threshold */
		priv->dm.ad_rx_ss_threshold = min(
			(priv->dm.ad_rx_ss + priv->dm.ad_rx_ss_before_switched) / 2,
			priv->dm.ad_max_rx_ss_threshold);

		if (priv->dm.ad_rx_ss < priv->dm.ad_rx_ss_before_switched)
			/* No improvement - switch back */
			rtl_rf_set_antenna(priv, new_antenna, true);
	} else {
		/* Evaluate if we shall switch antenna now */
		if (priv->dm.antenna == RTL_ANTENNA_MAIN &&
			priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_MAIN] <
			priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_AUX])
			rtl_rf_set_antenna(priv, RTL_ANTENNA_AUX, true);
		else if (priv->dm.antenna == RTL_ANTENNA_AUX &&
			priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_AUX] <
			priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_MAIN])
			rtl_rf_set_antenna(priv, RTL_ANTENNA_MAIN, true);
	}

	priv->dm.rx_ok_cnt = 0;
	priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_MAIN] = 0;
	priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_AUX] = 0;
}
