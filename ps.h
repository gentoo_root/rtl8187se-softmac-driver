#pragma once

#include "global.h"

/*
 * Inactive Power Saving
 */

void rtl_ips_set_state(struct rtl_priv *priv, bool state);

/*
 * Software Leisure Power Saving
 */
void rtl_lps_set_state(struct rtl_priv *priv, bool state);
