#pragma once

#include "bitops.h"

/*
 * RX descriptor flags
 */

/* Buffer size */
#define MASK_RX_DESC_BUF_SIZE		BITS(0, 11)
/* Integrity check value error */
#define BIT_RX_DESC_FLAG_ICV_ERR	BIT(12)
/* CRC32 error */
#define BIT_RX_DESC_FLAG_CRC32_ERR	BIT(13)
/* Power management frame */
#define BIT_RX_DESC_FLAG_PM		BIT(14)
/* Receive error */
#define BIT_RX_DESC_FLAG_RX_ERR		BIT(15)
/* Broadcast frame */
#define BIT_RX_DESC_FLAG_BCAST		BIT(16)
/* Physical address matched - destination address of packet matches our MAC address */
#define BIT_RX_DESC_FLAG_PAM		BIT(17)
/* Multicast frame */
#define BIT_RX_DESC_FLAG_MCAST		BIT(18)
/* Quality of Service frame */
#define BIT_RX_DESC_FLAG_QOS		BIT(19)
/* Rate index */
#define MASK_RX_DESC_RATE_IDX		BITS(20, 23)
#define BIT_RX_DESC_FLAG_TRSW		BIT(24)
/* Use short preamble */
#define BIT_RX_DESC_FLAG_SPLCP		BIT(25)
/* FIFO overflow */
#define BIT_RX_DESC_FLAG_FOF		BIT(26)
/* DMA failure */
#define BIT_RX_DESC_FLAG_DMA_FAIL	BIT(27)
/* Last fragment */
#define BIT_RX_DESC_FLAG_LS		BIT(28)
/* First fragment */
#define BIT_RX_DESC_FLAG_FS		BIT(29)
/* End of ring */
#define BIT_RX_DESC_FLAG_EOR		BIT(30)
/* Device owns descriptor */
#define BIT_RX_DESC_FLAG_OWN		BIT(31)

/*
 * TX descriptor flags
 */

/* Buffer size */
#define MASK_TX_DESC_BUF_SIZE		BITS(0, 11)
/* Disable hardware based encryption */
#define BIT_TX_DESC_FLAG_NO_ENC		BIT(15)
/* TX frame was ACKed */
#define BIT_TX_DESC_FLAG_TX_OK		BIT(15)
/* Use short preamble */
#define BIT_TX_DESC_FLAG_SPLCP		BIT(16)
#define BIT_TX_DESC_FLAG_RX_UNDER	BIT(16)
/* More fragments follow */
#define BIT_TX_DESC_FLAG_MOREFRAG	BIT(17)
/* Use CTS-to-self protection */
#define BIT_TX_DESC_FLAG_CTS		BIT(18)
/* RTS rate index */
#define MASK_TX_DESC_RTS_RATE_IDX	BITS(19, 22)
/* Use RTS/CTS protection */
#define BIT_TX_DESC_FLAG_RTS		BIT(23)
/* Rate index */
#define MASK_TX_DESC_RATE_IDX		BITS(24, 27)
/* Last segment of the frame */
#define BIT_TX_DESC_FLAG_LS		BIT(28)
/* First segment of the frame */
#define BIT_TX_DESC_FLAG_FS		BIT(29)
#define BIT_TX_DESC_FLAG_DMA		BIT(30)
/* Device owns descriptor */
#define BIT_TX_DESC_FLAG_OWN		BIT(31)
