#include "main.h"
#include "reg.h"
#include "io.h"
#include "hw.h"
#include "rx.h"
#include "tx.h"
#include "rings.h"
#include "rf.h"
#include "regd.h"
#include "ps.h"
#include "dm.h"
#include <linux/eeprom_93cx6.h>
#include <linux/etherdevice.h>
#include <linux/interrupt.h>

MODULE_AUTHOR("Maxim Mikityanskiy <maxtram95@gmail.com>");
MODULE_DESCRIPTION("RTL8187SE PCI wireless driver");
MODULE_LICENSE("GPL");

static DEFINE_PCI_DEVICE_TABLE(rtl_pci_table) = {
	{ PCI_DEVICE(PCI_VENDOR_ID_REALTEK, 0x8199) },
	{}
};

MODULE_DEVICE_TABLE(pci, rtl_pci_table);

static bool rfkill_enabled = true;
module_param(rfkill_enabled, bool, S_IRUGO);

#define pci_err(pdev, fmt, args...) pr_err("PCI %s: " fmt, pci_name(pdev), ##args)

static irqreturn_t rtl_isr(int irq, void *dev_id)
{
	RTL_PRIV(dev_id);
	u32 isr;
	static const u32 isr_mask_tx_queue[RTL_TX_QUEUE_COUNT] = {
		BIT_IMR_TMG_OK | BIT_IMR_TMG_ERR,
		BIT_IMR_TBK_OK | BIT_IMR_TBK_ERR,
		BIT_IMR_TBE_OK | BIT_IMR_TBE_ERR,
		BIT_IMR_TVI_OK | BIT_IMR_TVI_ERR,
		BIT_IMR_TVO_OK | BIT_IMR_TVO_ERR,
		BIT_IMR_THP_OK | BIT_IMR_THP_ERR,
		BIT_IMR_TBC_OK | BIT_IMR_TBC_ERR,
	};
	size_t i;

	/* Interrupt Status Register */
	isr = rtl_ioread32(priv, REG_ISR);

	/* HW disappeared */
	if (unlikely(isr == 0xffffffff))
		return IRQ_HANDLED;

	/* Shared IRQ */
	if (!isr)
		return IRQ_NONE;
	
	rtl_iowrite32(priv, REG_ISR, isr);

	if (unlikely(isr & BIT_IMR_RX_UNAVAIL))
		msg_warn(priv, "RX descriptor unavailable\n");

	if (unlikely(isr & BIT_IMR_RX_OVERFLOW))
		msg_warn(priv, "RX FIFO overflow\n");

	if (unlikely(isr & BIT_IMR_TX_OVERFLOW))
		/* For some reason this interrupt appears very often and
		 * floods dmesg, so hide it for now
		 */
		msg_dbg(priv, "TX FIFO overflow\n");

	if (isr & (BIT_IMR_RX_OK | BIT_IMR_RX_ERR | BIT_IMR_RQOS_OK))
		rtl_isr_rx(priv);

	for (i = 0; i < RTL_TX_QUEUE_COUNT; i++)
		if (isr & isr_mask_tx_queue[i])
			rtl_isr_tx(priv, i);

	if (isr & BIT_IMR_BEACON_TIMEOUT) {
		if (priv->vif && priv->nic.beacon_enabled) {
			struct rtl_vif *vif_priv = (struct rtl_vif *)&priv->vif->drv_priv;
			tasklet_schedule(&vif_priv->beacon_tasklet);
		}
	}

	return IRQ_HANDLED;
}

void rtl_update_channel(struct rtl_priv *priv)
{
	rtl_rf_set_channel(priv, ieee80211_frequency_to_channel(priv->hw->conf.channel->center_freq));
}

static void rtl_watchdog_cb(struct work_struct *work)
{
	struct delayed_work *dwork = container_of(work, struct delayed_work, work);
	struct rtl_wdt *wdt = container_of(dwork, struct rtl_wdt, work);
	struct rtl_priv *priv = container_of(wdt, struct rtl_priv, wdt);

	if (priv->wdt.running)
		ieee80211_queue_delayed_work(priv->hw, &priv->wdt.work, round_jiffies_relative(RTL_WATCHDOG_INTERVAL));

	msg_dbg(priv, "Watchdog\n");
	if (++priv->wdt.ticks % 2)
		rtl_dm_watchdog(priv);
	rtl_dm_antenna_diversity(priv);
}

static void rtl_watchdog_start(struct rtl_priv *priv)
{
	priv->wdt.running = true;
	ieee80211_queue_delayed_work(priv->hw, &priv->wdt.work, round_jiffies_relative(RTL_WATCHDOG_INTERVAL));
}

static void rtl_watchdog_stop(struct rtl_priv *priv)
{
	priv->wdt.running = false;
	cancel_delayed_work_sync(&priv->wdt.work);
}

static int rtl_op_start(struct ieee80211_hw *hw)
{
	RTL_PRIV(hw);
	int err;

	spin_lock_init(&priv->nic.rf_lock);
	spin_lock_init(&priv->ps.ps_lock);
	priv->ps.ips_active = false;
	priv->ps.lps_active = false;
	priv->dm.smoothed_signal_strength = -1;
	priv->dm.lowered_tx_pwr = false;
	priv->dm.rate_idx = 0;
	priv->dm.last_cck = false;
	priv->dm.fallback_votes = 0;
	priv->dm.upgrade_votes = 0;
	priv->dm.thermal_meter = priv->eeprom.thermal_meter;
	priv->dm.rx_ok_cnt = 0;
	priv->dm.ad_rx_ss = -1;
	priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_MAIN] = 0;
	priv->dm.rx_ok_cnt_antenna[RTL_ANTENNA_AUX] = 0;
	priv->dm.ad_switched_checking = false;
	priv->dm.ad_rx_ss_threshold = 20;
	priv->dm.ad_max_rx_ss_threshold = 30;
	priv->dm.ad_rx_ss_before_switched = 0;
	priv->wdt.ticks = 0;

	err = rtl_init_rings(priv);
	if (err)
		return err;

	err = request_irq(priv->pdev->irq, rtl_isr, IRQF_SHARED, KBUILD_MODNAME, hw);
	if (err) {
		msg_err(priv, "failed to register IRQ handler\n");
		rtl_deinit_rings(priv);
		return err;
	}

	/* Initialize MAC */
	rtl_hw_start(priv);

	/* Initialize RF */
	priv->nic.rf_power = false;
	rtl_rf_init_io(priv);
	rtl_rf_set_power_state(priv, true);
	rtl_rf_start(priv);
	rtl_rf_set_antenna(priv, priv->eeprom.default_antenna, priv->eeprom.antenna_diversity);
	rtl_rf_set_txpwr_tracking(priv, priv->eeprom.txpwr_tracking);
	rtl_rf_set_initial_gain(priv, 4);

	/* Start working */
	rtl_set_irq_state(priv, true);
	rtl_set_tx_state(priv, true);
	rtl_set_rx_state(priv, true);

	rtl_watchdog_start(priv);

	return 0;
}

static void rtl_op_stop(struct ieee80211_hw *hw)
{
	RTL_PRIV(hw);

	rtl_watchdog_stop(priv);

	rtl_set_rx_state(priv, false);
	rtl_set_tx_state(priv, false);
	rtl_set_irq_state(priv, false);

	rtl_rf_set_power_state(priv, false);
	rtl_set_power(priv, false);

	free_irq(priv->pdev->irq, hw);

	rtl_deinit_rings(priv);
}

static void rtl_beacon_tasklet(unsigned long data)
{
	struct rtl_priv *priv = (struct rtl_priv *)data;
	rtl_tx_beacon(priv);
}

static int rtl_op_up(struct ieee80211_hw *hw, struct ieee80211_vif *vif)
{
	RTL_PRIV(hw);
	struct rtl_vif *vif_priv;

	switch (vif->type) {
	case NL80211_IFTYPE_STATION:
	case NL80211_IFTYPE_ADHOC:
	case NL80211_IFTYPE_AP:
		break;
	default:
		msg_err(priv, "Unsupported interface type\n");
		return -EOPNOTSUPP;
	}

	if (priv->vif) {
		msg_err(priv, "Only one interface supported at a time\n");
		return -EBUSY;
	}

	priv->vif = vif;

	vif_priv = (struct rtl_vif *)&vif->drv_priv;
	vif_priv->hw = hw;
	tasklet_init(&vif_priv->beacon_tasklet, rtl_beacon_tasklet, (unsigned long)priv);

	rtl_set_beacon_enabled(priv, false);
	rtl_set_beacon_interval(priv, 100);
	rtl_set_mac_address(priv, vif->addr);
	rtl_set_opmode(priv, vif->type);

	return 0;
}

static void rtl_op_down(struct ieee80211_hw *hw, struct ieee80211_vif *vif)
{
	RTL_PRIV(hw);

	if (WARN_ON(priv->vif != vif))
		return;

	priv->vif = NULL;
	rtl_set_opmode(priv, NL80211_IFTYPE_UNSPECIFIED);
}

static int rtl_op_config(struct ieee80211_hw *hw, u32 changed)
{
	RTL_PRIV(hw);

	if (changed & IEEE80211_CONF_CHANGE_CHANNEL)
		rtl_update_channel(priv);

	if (changed & IEEE80211_CONF_CHANGE_RETRY_LIMITS)
		rtl_set_retry_limits(priv, hw->conf.short_frame_max_tx_count, hw->conf.long_frame_max_tx_count);

	if (changed & IEEE80211_CONF_CHANGE_IDLE)
		rtl_ips_set_state(priv, !(hw->conf.flags & IEEE80211_CONF_IDLE));

//	if (changed & IEEE80211_CONF_CHANGE_PS)
//		rtl_lps_set_state(priv, !(hw->conf.flags & IEEE80211_CONF_PS));

	return 0;
}

static u64 rtl_op_prepare_multicast(struct ieee80211_hw *hw, struct netdev_hw_addr_list *mc_list)
{
	return netdev_hw_addr_list_count(mc_list);
}

static void rtl_op_configure_filter(struct ieee80211_hw *hw, unsigned changed, unsigned *flags, u64 multicast)
{
	RTL_PRIV(hw);
	u32 rx_conf = priv->nic.rx_conf;

	*flags &= FIF_ALLMULTI | FIF_FCSFAIL | FIF_BCN_PRBRESP_PROMISC | FIF_CONTROL | FIF_OTHER_BSS;

	if (*flags & FIF_ALLMULTI || multicast > 0) {
		msg_dbg(priv, "Enabling RX multicast\n");
		rx_conf |= BIT_RX_CONF_MULTICAST;
	} else {
		msg_dbg(priv, "Disabling RX multicast\n");
		rx_conf &= ~BIT_RX_CONF_MULTICAST;
	}

	if (changed & FIF_FCSFAIL) {
		if (*flags & FIF_FCSFAIL) {
			msg_dbg(priv, "Enabling RX failed checksum packets\n");
			rx_conf |= BIT_RX_CONF_FCS;
		} else {
			msg_dbg(priv, "Disabling RX failed checksum packets\n");
			rx_conf &= ~BIT_RX_CONF_FCS;
		}
	}

	if (changed & FIF_BCN_PRBRESP_PROMISC) {
		if (*flags & FIF_BCN_PRBRESP_PROMISC) {
			msg_dbg(priv, "Disabling RX BSSID match\n");
			rx_conf &= ~BIT_RX_CONF_BSSID;
		} else {
			msg_dbg(priv, "Enabling RX BSSID match\n");
			rx_conf |= BIT_RX_CONF_BSSID;
		}
	}

	if (changed & FIF_CONTROL) {
		if (*flags & FIF_CONTROL) {
			msg_dbg(priv, "Enabling RX control frames\n");
			rx_conf |= BIT_RX_CONF_CTRL;
		} else {
			msg_dbg(priv, "Disabling RX control frames\n");
			rx_conf &= ~BIT_RX_CONF_CTRL;
		}
	}

	if (changed & FIF_OTHER_BSS) {
		if (*flags & FIF_OTHER_BSS) {
			msg_dbg(priv, "Enabling RX packets from other BSS\n");
			rx_conf |= BIT_RX_CONF_MONITOR;
		} else {
			msg_dbg(priv, "Enabling RX packets from other BSS\n");
			rx_conf &= ~BIT_RX_CONF_MONITOR;
		}
	}

	rtl_set_rx_conf(priv, rx_conf);
}

static u64 rtl_op_get_tsf(struct ieee80211_hw *hw, struct ieee80211_vif *vif)
{
	return rtl_get_tsf(hw->priv);
}

static void rtl_op_bss_info_changed(struct ieee80211_hw *hw, struct ieee80211_vif *vif, struct ieee80211_bss_conf *info, u32 changed)
{
	RTL_PRIV(hw);

	if (changed & BSS_CHANGED_BSSID)
		rtl_set_bssid(priv, info->bssid);

	if (changed & BSS_CHANGED_BEACON_ENABLED)
		rtl_set_beacon_enabled(priv, info->enable_beacon);

	if (changed & BSS_CHANGED_BEACON_INT)
		rtl_set_beacon_interval(priv, info->beacon_int);

	if (changed & BSS_CHANGED_BASIC_RATES)
		rtl_set_brsr(priv, info->basic_rates);
}

static int rtl_op_conf_tx(struct ieee80211_hw *hw, struct ieee80211_vif *vif, u16 queue, const struct ieee80211_tx_queue_params *params)
{
	RTL_PRIV(hw);

	if (queue >= IEEE80211_NUM_ACS)
		return -EINVAL;

	memcpy(&priv->qos_params[queue], params, sizeof(*params));
	rtl_set_qos(priv, queue);

	return 0;
}

static void rtl_op_rfkill_poll(struct ieee80211_hw *hw)
{
	RTL_PRIV(hw);
	bool rfkill_state = rtl_radio_state(priv);

	if (unlikely(priv->rfkill_state != rfkill_state)) {
		priv->rfkill_state = rfkill_state;
		msg_dbg(priv, "RFKILL turned %s\n", rfkill_state ? "on" : "off");
		wiphy_rfkill_set_hw_state(priv->hw->wiphy, !rfkill_state);
	}
}

static int rtl_op_sta_add(struct ieee80211_hw *hw, struct ieee80211_vif *vif, struct ieee80211_sta *sta)
{
	return 0;
}

static int rtl_op_sta_remove(struct ieee80211_hw *hw, struct ieee80211_vif *vif, struct ieee80211_sta *sta)
{
	return 0;
}

static void rtl_op_sta_notify(struct ieee80211_hw *hw, struct ieee80211_vif *vif, enum sta_notify_cmd notify_cmd, struct ieee80211_sta *sta)
{
}

static const struct ieee80211_ops rtl_ops = {
	.start			= rtl_op_start,
	.stop			= rtl_op_stop,
	.add_interface		= rtl_op_up,
	.remove_interface	= rtl_op_down,
	.tx			= rtl_op_tx,
	.flush			= rtl_op_flush,
	.config			= rtl_op_config,
	.prepare_multicast	= rtl_op_prepare_multicast,
	.configure_filter	= rtl_op_configure_filter,
	.get_tsf		= rtl_op_get_tsf,
	.bss_info_changed	= rtl_op_bss_info_changed,
	.conf_tx		= rtl_op_conf_tx,
	.rfkill_poll		= rtl_op_rfkill_poll,
	.sta_add		= rtl_op_sta_add,
	.sta_remove		= rtl_op_sta_remove,
	.sta_notify		= rtl_op_sta_notify,
};

static bool rtl_read_eeprom(struct rtl_priv *priv)
{
	struct eeprom_93cx6 eeprom;
	u16 data;
	u8 mac_addr[ETH_ALEN];
	size_t i;

	msg_dbg(priv, "Probing NIC\n");
	rtl_begin_eeprom(priv, &eeprom);

	/* Read EEPROM ID */
	eeprom_93cx6_read(&eeprom, 0x00, &data);
	if (data != 0x8129) {
		msg_err(priv, "Invalid EEPROM contents\n");
		return false;
	}

	/* Read RF chip ID */
	eeprom_93cx6_read(&eeprom, 0x06, &data);
	if ((data & 0xff) != 9) {	/* RTL8225 */
		msg_err(priv, "Unsupported RF chip ID: %d\n", data & 0xff);
		return false;
	}

	/* Read hwaddr */
	eeprom_93cx6_multiread(&eeprom, 0x07, (__le16 *)mac_addr, 3);
	if (!is_valid_ether_addr(mac_addr)) {
		msg_warn(priv, "Invalid hwaddr, generating random MAC address\n");
		eth_random_addr(mac_addr);
	}
	SET_IEEE80211_PERM_ADDR(priv->hw, mac_addr);

	/* Read country code */
	eeprom_93cx6_read(&eeprom, 0x17, &data);
	priv->eeprom.country_code = SHIFTOUT(data, 0xff);
	msg_dbg(priv, "Channel plan is %d\n", priv->eeprom.country_code);

	/* Read EEPROM version */
	eeprom_93cx6_read(&eeprom, 0x1e, &data);
	msg_dbg(priv, "EEPROM version %x\n", data);

	/* Read CCK TX power values */
	for (i = 0; i < 14; i += 2) {
		u16 txpwr;
		u8 pwr1, pwr2;
		eeprom_93cx6_read(&eeprom, 0x30 + (i>>1), &txpwr);
		pwr1 = SHIFTOUT(txpwr, BITS(0, 7));
		pwr2 = SHIFTOUT(txpwr, BITS(8, 15));
		priv->channels[i].hw_value = SHIFTIN(pwr1, BITS(0, 7));
		priv->channels[i+1].hw_value = SHIFTIN(pwr2, BITS(0, 7));
		msg_dbg(priv, "Channel %d CCK pwr %d\n", i+1, pwr1);
		msg_dbg(priv, "Channel %d CCK pwr %d\n", i+2, pwr2);
	}

	/* Read OFDM TX power values */
	for (i = 0; i < 14; i += 2) {
		u16 txpwr;
		u8 pwr1, pwr2;
		eeprom_93cx6_read(&eeprom, 0x20 + (i>>1), &txpwr);
		pwr1 = SHIFTOUT(txpwr, BITS(0, 7));
		pwr2 = SHIFTOUT(txpwr, BITS(8, 15));
		priv->channels[i].hw_value |= SHIFTIN(pwr1, BITS(8, 15));
		priv->channels[i+1].hw_value |= SHIFTIN(pwr2, BITS(8, 15));
		msg_dbg(priv, "Channel %d OFDM pwr %d\n", i+1, pwr1);
		msg_dbg(priv, "Channel %d OFDM pwr %d\n", i+2, pwr2);
	}

	/* Read crystal calibration and thermal meter info */
	eeprom_93cx6_read(&eeprom, 0x3e, &data);
	priv->eeprom.xtalcal_enable = SHIFTOUT(data, BIT(12));
	priv->eeprom.xtalcal_xout = SHIFTOUT(data, BITS(0, 3)); /* 0-7.5pF, units 0.5pF */
	priv->eeprom.xtalcal_xin = SHIFTOUT(data, BITS(4, 7));
	priv->eeprom.txpwr_tracking = SHIFTOUT(data, BIT(13)); /* thermal meter enable */
	priv->eeprom.thermal_meter = SHIFTOUT(data, BITS(8, 11));

	/* Read software antenna diversity config */
	eeprom_93cx6_read(&eeprom, 0x3f, &data);
	priv->eeprom.antenna_diversity = SHIFTOUT(data, BITS(8, 9)) == 1;
	priv->eeprom.default_antenna = SHIFTOUT(data, BITS(10, 11)) == 1;

	/* Finish reading from EEPROM */
	rtl_end_eeprom(priv);

	return true;
}

static void rtl_rfk_init(struct rtl_priv *priv)
{
	if (rfkill_enabled) {
		priv->rfkill_state = rtl_radio_state(priv);
		wiphy_rfkill_set_hw_state(priv->hw->wiphy, !priv->rfkill_state);
		wiphy_rfkill_start_polling(priv->hw->wiphy);
	} else
		wiphy_rfkill_set_hw_state(priv->hw->wiphy, false);
}

#define RTL_CHANNEL_2GHZ(freq) { .band = IEEE80211_BAND_2GHZ, .center_freq = (freq) }

static struct ieee80211_channel rtl_channels[] = {
	RTL_CHANNEL_2GHZ(2412),
	RTL_CHANNEL_2GHZ(2417),
	RTL_CHANNEL_2GHZ(2422),
	RTL_CHANNEL_2GHZ(2427),
	RTL_CHANNEL_2GHZ(2432),
	RTL_CHANNEL_2GHZ(2437),
	RTL_CHANNEL_2GHZ(2442),
	RTL_CHANNEL_2GHZ(2447),
	RTL_CHANNEL_2GHZ(2452),
	RTL_CHANNEL_2GHZ(2457),
	RTL_CHANNEL_2GHZ(2462),
	RTL_CHANNEL_2GHZ(2467),
	RTL_CHANNEL_2GHZ(2472),
	RTL_CHANNEL_2GHZ(2484),
};

#define RTL_BITRATE(idx, rate) { .bitrate = (rate), .hw_value = (idx) }

static struct ieee80211_rate rtl_bitrates[] = {
	RTL_BITRATE(0, 10),
	RTL_BITRATE(1, 20),
	RTL_BITRATE(2, 55),
	RTL_BITRATE(3, 110),
	RTL_BITRATE(4, 60),
	RTL_BITRATE(5, 90),
	RTL_BITRATE(6, 120),
	RTL_BITRATE(7, 180),
	RTL_BITRATE(8, 240),
	RTL_BITRATE(9, 360),
	RTL_BITRATE(10, 480),
	RTL_BITRATE(11, 540),
};

static int rtl_pci_probe(struct pci_dev *pdev, const struct pci_device_id *id)
{
	int err;
	void __iomem *iomem;
	struct ieee80211_hw *hw;
	struct rtl_priv *priv;

	err = pci_enable_device(pdev);
	if (err) {
		pci_err(pdev, "Cannot enable new PCI device\n");
		goto exit;
	}

	err = pci_request_regions(pdev, KBUILD_MODNAME);
	if (err) {
		pci_err(pdev, "Cannot obtain PCI resources\n");
		goto exit_disable;
	}

	err = pci_set_dma_mask(pdev, DMA_BIT_MASK(32));
	if (err) {
		pci_err(pdev, "Unable to obtain suitable DMA\n");
		goto exit_release;
	}
	err = pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(32));
	if (err) {
		pci_err(pdev, "Unable to obtain suitable DMA\n");
		goto exit_release;
	}

	/* Disable the RETRY_TIMEOUT register (0x41) to keep PCI TX retries from
	 * interfering with C3 CPU state.
	 */
	pci_write_config_byte(pdev, 0x41, 0x00);

	pci_set_master(pdev);

	iomem = pci_iomap(pdev, 1, pci_resource_len(pdev, 1));
	if (!iomem) {
		pci_err(pdev, "Cannot map device memory\n");
		err = -ENOMEM;
		goto exit_release;
	}

	hw = ieee80211_alloc_hw(sizeof(*priv), &rtl_ops);
	if (!hw) {
		pci_err(pdev, "Cannot allocate ieee80211 device\n");
		err = -ENOMEM;
		goto exit_unmap;
	}

	SET_IEEE80211_DEV(hw, &pdev->dev);

	priv = hw->priv;
	priv->pdev = pdev;
	priv->hw = hw;
	priv->iomem = iomem;
	pci_set_drvdata(pdev, priv);

	INIT_DELAYED_WORK(&priv->wdt.work, rtl_watchdog_cb);

	BUILD_BUG_ON(sizeof(priv->channels) != sizeof(rtl_channels));
	BUILD_BUG_ON(sizeof(priv->bitrates) != sizeof(rtl_bitrates));

	memcpy(priv->channels, rtl_channels, sizeof(rtl_channels));
	memcpy(priv->bitrates, rtl_bitrates, sizeof(rtl_bitrates));

	priv->band.band = IEEE80211_BAND_2GHZ;
	priv->band.channels = priv->channels;
	priv->band.n_channels = ARRAY_SIZE(priv->channels);
	priv->band.bitrates = priv->bitrates;
	priv->band.n_bitrates = ARRAY_SIZE(priv->bitrates);
	hw->wiphy->bands[IEEE80211_BAND_2GHZ] = &priv->band;

	hw->flags =
		IEEE80211_HW_SIGNAL_DBM |
		IEEE80211_HW_RX_INCLUDES_FCS |
		IEEE80211_HW_SUPPORTS_PS |
		IEEE80211_HW_PS_NULLFUNC_STACK |
//		IEEE80211_HW_SUPPORTS_DYNAMIC_PS |
		IEEE80211_HW_REPORTS_TX_ACK_STATUS;
	hw->vif_data_size = sizeof(struct rtl_vif);
	hw->wiphy->interface_modes =
		BIT(NL80211_IFTYPE_STATION) |
		BIT(NL80211_IFTYPE_ADHOC) |
		BIT(NL80211_IFTYPE_AP);
	hw->wiphy->rts_threshold = 2347;
	hw->queues = 4;
	hw->channel_change_time = 100;
	hw->max_listen_interval = 10;
	hw->max_rate_tries = 4;
	hw->max_rates = 1;

	if ((rtl_ioread32(priv, REG_TX_CONF) & MASK_TX_CONF_HWVER) != BIT_TX_CONF_HWVER_R8187vD_B) {
		pci_err(pdev, "NIC is not supported\n");
		err = -ENODEV;
		goto exit_free;
	}

	if (!rtl_read_eeprom(priv)) {
		err = -EIO;
		goto exit_free;
	}

	err = ieee80211_register_hw(hw);
	if (err) {
		pci_err(pdev, "Cannot register device\n");
		goto exit_free;
	}

	msg_info(priv, "Registered RTL8187SE hwaddr %pM\n", hw->wiphy->perm_addr);

	rtl_rfk_init(priv);
	rtl_regd_init(priv);

	return 0;

exit_free:
	ieee80211_free_hw(hw);

exit_unmap:
	pci_iounmap(pdev, iomem);

exit_release:
	pci_release_regions(pdev);

exit_disable:
	pci_disable_device(pdev);

exit:
	pci_set_drvdata(pdev, NULL);
	return err;
}

static void rtl_pci_remove(struct pci_dev *pdev)
{
	struct rtl_priv * const priv = pci_get_drvdata(pdev);

	if (priv) {
		rtl_watchdog_stop(priv);
		ieee80211_unregister_hw(priv->hw);
		pci_iounmap(pdev, priv->iomem);
		ieee80211_free_hw(priv->hw);
		pci_release_regions(pdev);
		pci_disable_device(pdev);
	}
}

static int rtl_pci_suspend(struct pci_dev *pdev, pm_message_t state)
{
	pci_save_state(pdev);
	pci_disable_device(pdev);
	pci_set_power_state(pdev, pci_choose_state(pdev, state));
	return 0;
}

static int rtl_pci_resume(struct pci_dev *pdev)
{
	int err;

	pci_set_power_state(pdev, PCI_D0);
	err = pci_enable_device(pdev);
	if (err) {
		pci_err(pdev, "Cannot enable PCI device after resume\n");
		return err;
	}
	pci_restore_state(pdev);

	/* Suspend resets the PCI configuration space, so we have to redisable
	 * the RETRY_TIMEOUT register (0x41) to keep PCI TX retries from
	 * interfering with C3 CPU state.
	 */
	pci_write_config_byte(pdev, 0x41, 0x00);

	return 0;
}

static void rtl_pci_shutdown(struct pci_dev *pdev)
{
	pci_disable_device(pdev);
}

static struct pci_driver rtl_pci_driver = {
	.name		= KBUILD_MODNAME,
	.id_table	= rtl_pci_table,
	.probe		= rtl_pci_probe,
	.remove		= rtl_pci_remove,
	.suspend	= rtl_pci_suspend,
	.resume		= rtl_pci_resume,
	.shutdown	= rtl_pci_shutdown,
};

module_pci_driver(rtl_pci_driver);
