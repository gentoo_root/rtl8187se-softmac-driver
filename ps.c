#include "ps.h"
#include "rf.h"

static bool ips_enabled = true;
module_param(ips_enabled, bool, S_IRUGO);

static bool lps_enabled = true;
module_param(lps_enabled, bool, S_IRUGO);

void rtl_ips_set_state(struct rtl_priv *priv, bool state)
{
	unsigned long flags;

	msg_dbg(priv, "Setting power state by IPS: %s\n", state ? "on" : "off");

	spin_lock_irqsave(&priv->ps.ps_lock, flags);
	priv->ps.ips_active = !state && ips_enabled;
	rtl_rf_set_power_state(priv, !(priv->ps.ips_active || priv->ps.lps_active));
	spin_unlock_irqrestore(&priv->ps.ps_lock, flags);
}

void rtl_lps_set_state(struct rtl_priv *priv, bool state)
{
	unsigned long flags;

	msg_dbg(priv, "Setting power state by LPS: %s\n", state ? "on" : "off");

	spin_lock_irqsave(&priv->ps.ps_lock, flags);
	priv->ps.lps_active = !state && lps_enabled;
	rtl_rf_set_power_state(priv, !(priv->ps.ips_active || priv->ps.lps_active));
	spin_unlock_irqrestore(&priv->ps.ps_lock, flags);
}
