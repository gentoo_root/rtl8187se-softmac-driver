#pragma once

#include "global.h"

/*
 * RX/TX descriptor rings
 */

int rtl_init_rings(struct rtl_priv *priv);
void rtl_deinit_rings(struct rtl_priv *priv);
