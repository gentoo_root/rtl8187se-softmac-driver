#pragma once

#include "global.h"
#include <linux/nl80211.h>

/*
 * NIC configuration functions
 */

enum rtl_eeprom_mode {
	RTL_EEPROM_MODE_NORMAL,
	RTL_EEPROM_MODE_LOAD,
	RTL_EEPROM_MODE_PROGRAM,
	RTL_EEPROM_MODE_CONFIG,
};

/* EEPROM access */
struct eeprom_93cx6;
void rtl_begin_eeprom(struct rtl_priv *priv, struct eeprom_93cx6 *eeprom);
void rtl_end_eeprom(struct rtl_priv *priv);

/* Analog parameters access */
void rtl_unlock_anaparam(struct rtl_priv *priv);
void rtl_lock_anaparam(struct rtl_priv *priv);

/* Radio power switch */
bool rtl_radio_state(struct rtl_priv *priv);

/* MAC parameters */
void rtl_set_opmode(struct rtl_priv *priv, enum nl80211_iftype iftype);
void rtl_set_tx_state(struct rtl_priv *priv, bool state);
void rtl_set_rx_state(struct rtl_priv *priv, bool state);
void rtl_set_irq_state(struct rtl_priv *priv, bool state);
void rtl_set_mac_address(struct rtl_priv *priv, const u8 *mac);
void rtl_set_bssid(struct rtl_priv *priv, const u8 *bssid);
u64 rtl_get_tsf(struct rtl_priv *priv);
void rtl_set_beacon_enabled(struct rtl_priv *priv, bool state);
void rtl_set_beacon_interval(struct rtl_priv *priv, u16 value);
void rtl_set_atim_window(struct rtl_priv *priv, u16 value);
void rtl_set_rx_conf(struct rtl_priv *priv, u32 rx_conf);
void rtl_set_tx_conf(struct rtl_priv *priv, u32 tx_conf);
void rtl_set_qos(struct rtl_priv *priv, u8 queue);
void rtl_set_retry_limits(struct rtl_priv *priv, u8 retry_short, u8 retry_long);
void rtl_set_brsr(struct rtl_priv *priv, u16 brsr);

/* Descriptor rings */
void rtl_set_rx_ring(struct rtl_priv *priv);
void rtl_set_tx_rings(struct rtl_priv *priv);
size_t rtl_get_tx_ring_pointer(struct rtl_priv *priv, u8 queue);
void rtl_tx_dma(struct rtl_priv *priv, u8 queue);
bool rtl_flush(struct rtl_priv *priv, u8 queue, int timeout);

/* Hardware initialization */
void rtl_set_power(struct rtl_priv *priv, bool state);
int rtl_hw_start(struct rtl_priv *priv);
